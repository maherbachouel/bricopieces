jQuery(document).ready(function ($) {
    /*Init Datatable*/
    $("#admindatatable").dataTable({
        aLengthMenu: [
            [-1,10, 25, 50, 100 ], [ "All",10, 25, 50, 100]
        ]
    });
    /*delete */
    $('.confirm-modal').click(function () {
        var href = $(this).attr('data-href');
        var id = $(this).attr('data-id');
        console.log(id);
        console.log(href);
        $.confirm({
            title: 'Warning',
            content: 'Are you sure you want to delete it?',
            type: 'red',
            typeAnimated: false,
            theme: 'supervan',
            closeIcon: true,
            animation: 'scale',
            icon: 'fa fa-warning',
            buttons: {
                yes: {
                    text: 'Yes',
                    btnClass: 'btn-red',
                    action: function () {
                        location.href = href + '/' + id;
                    }
                }, close: function () {
                }
            }
        });
    });
    setTimeout(function(){
        $("#select_country").change();
    },0);
    $('#select_country').change(function () {
        var country = $(this).val();
        $.ajax({
            beforeSend: function (xhrObj) {
                xhrObj.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                xhrObj.setRequestHeader("Content-Type", "application/json");
                xhrObj.setRequestHeader("Accept", "application/json");
            },
            url: $(this).attr('data-action'),
            method: 'get',
            data: {
                country: country
            },
            success: function (result) {
                result = JSON.parse(result);
                if (result.success) {
                    var data = result.city;
                    var html = '';
                    for (var i = 0; i < data.length; i++) {
                        html += '<option value="' + data[0].city_id + '">' + data[0].name + '</option>'
                    }
                    $('#select_city').html(html);
                } else {
                    $('#select_city').html('');
                }
            },
        });
    })
    $('.keyup-meta').keyup(function () {
        var val = $(this).val();
        val = val
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
        $('.permalink-set').val(val.toLowerCase());
    })
});