<div class="page-title">
    <div class="title-env">
        <h1 class="title">Liste des pratiques</h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa-home"></i>Tableau de bord</a>
            </li>
            <li class="active ms-hover">
                <strong>Liste des pratiques</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="xe-widget xe-counter xe-counter-blue" data-count=".num" data-from="0" data-to="<?php echo count($pratiques); ?>" data-duration="2">
            <div class="xe-icon">
                <i class="linecons-desktop"></i>
            </div>
            <div class="xe-label">
                <strong class="num"><?php echo count($pratiques); ?></strong>
                <span>Nombre des Vidéos</span>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="xe-widget xe-counter" data-count=".num" data-from="0" data-to="<?php echo $published; ?>" data-duration="2">
            <div class="xe-icon">
                <i class="linecons-desktop"></i>
            </div>
            <div class="xe-label">
                <strong class="num"><?php echo $published; ?></strong>
                <span>Vidéos publier</span>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="xe-widget xe-counter xe-counter-red" data-count=".num" data-from="0" data-to="<?php echo $draft ; ?>" data-duration="2">
            <div class="xe-icon">
                <i class="linecons-desktop"></i>
            </div>
            <div class="xe-label">
                <strong class="num"><?php echo $draft; ?></strong>
                <span>Vidéos non publier</span>
            </div>
        </div>
    </div>
</div>
<?php if (count($pratiques)) { ?>
    <div class="row">
        <div class="col-sm-12">
            <!--Basic Setup -->
            <div class="panel panel-default">
                <div class="panel-body">
                    <table id="admindatatable" class="dataTable table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Banner</th>
                            <th>Titre</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($pratiques as $practice) {
                            ?>
                            <tr>
                                <td><?php echo $practice->id; ?></td>
                                <td>
                                    <video class="cover-admin" autoplay loop>
                                        <source src="<?php echo base_url() . $practice->banner; ?>" type="video/mp4"/>
                                        Ici l'alternative à la vidéo : un lien de téléchargement, un message, etc.
                                    </video>
                                </td>
                                <td><?php echo $practice->title; ?></td>
                                <td><?php if ($practice->status == 0) {
                                        echo "Draft";
                                    } else {
                                        echo "Published";
                                    } ?></td>
                                <td>
                                    <a href="<?php echo base_url(); ?>admin/pratiques/edit/<?php echo $practice->id; ?>"
                                       data-id="<?php echo $practice->id; ?>"><i
                                            class="icon-admin linecons-pencil"></i></a>
                                    <a href="#" class="confirm-modal" data-href="<?php echo base_url(); ?>admin/pratiques/delete"
                                       data-id="<?php echo $practice->id; ?>"><i
                                            class="icon-admin fa-times-circle-o"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php } else {
    ?>
    <div class="no-data">
        <div class="no-data-icon">
            <i class="fa fa-exclamation"></i>
        </div>
        <p class="no-data-text">Aucun élément dans la liste</p>
        <a href="<?php echo base_url(); ?>admin/pratiques/add" class="no-data-link">Ajouter une pratique</a>
    </div>
<?php } ?>