<div id="content" class="site-content">
    <div class="col-full">
        <div class="row">
            <nav class="woocommerce-breadcrumb">
                <a href="<?php echo base_url(); ?>">Accueil</a>
                <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                <a href="<?php echo base_url().'blog'; ?>">Blog</a>
                <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span><?php echo $article->title;?>
            </nav>
            <!-- .woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <article class="post format-image">
                        <div class="media-attachment">
                            <div class="post-thumbnail">
                                <img width="1433" height="560" title="<?php echo $article->title;?>" alt="<?php echo $article->title;?>" class="wp-post-image" src="<?php echo base_url() . $article->banner ?>">
                            </div>
                        </div>
                        <header class="entry-header">
                            <h1 class="entry-title"><?php echo $article->title;?>
                            </h1>
                            <!-- .entry-title -->
                            <div class="entry-meta">
                                <!-- .cat-links -->
                                <span class="author">
                                                <a rel="bookmark" href="javascript:void(0)">
                                                    <time class="entry-date published"><?php echo date("d/M/Y", strtotime($article->updated_at)); ?></time>
                                                </a>
                                            </span>
                                <!-- .posted-on -->
                                <span class="author">
                                                <a rel="author" title="Admin" href="javascript:void(0)">Admin</a>
                                            </span>
                                <!-- .author -->
                            </div>
                            <!-- .entry-meta -->
                        </header>
                        <!-- .entry-header -->
                        <div class="entry-content" itemprop="articleBody">
                            <?php echo $article->description;?>
                        </div>
                        <!-- .entry-content -->
                    </article>
                    <!-- .post -->
                    <div class="post-author-info">
                        <div class="media">
                            <!-- .media-left -->
                            <div class="media-body">
                                <h4 class="media-heading">
                                    <a href="javascript:void(0)">Administrateur</a>
                                </h4>
                            </div>
                            <!-- .media-body -->
                        </div>
                        <!-- .media -->
                    </div>
                    <!-- .post-author-info -->
                    <!-- /.post-navigation -->
                    <!-- .comments-area -->
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
            <!-- .widget_tag_cloud -->
        </div>
        <!-- .sidebar-blog -->
    </div>
    <!-- .row -->
</div>