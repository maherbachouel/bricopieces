<div id="content" class="site-content">
    <div class="col-full">
        <div class="row">
            <nav class="woocommerce-breadcrumb">
                <a href="<?php echo base_url(); ?>">Accueil</a>
                <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>Connexion
            </nav>
            <!-- .woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="type-page hentry">
                        <div class="entry-content">
                            <div class="woocommerce container">
                                <div id="" class="u-columns">
                                    <div class="u-column1">
                                        <h2>Connexion</h2>
                                        <form method="post" class="woocomerce-form woocommerce-form-login login" action="<?php echo base_url().'connexion'?>">
                                            <p class="before-login-text">
                                            </p>
                                            <p class="form-row form-row-wide">
                                                <label for="username">Adresse email
                                                    <span class="required">*</span>
                                                </label>
                                                <input type="email" class="input-text" name="email" id="email" required />
                                            </p>
                                            <p class="form-row form-row-wide">
                                                <label for="password">Mot de passe
                                                    <span class="required">*</span>
                                                </label>
                                                <input class="input-text" type="password" name="password" id="password" required />
                                            </p>
											<?php if($notification){ ?>
											<p class="form-row form-row-wide" style="color:red;">
                                               Merci de vérifier votre adresse mail / mot de passe où contacter l'administrateur de site.
                                            </p>
											<?php } ?>
                                            <p class="form-row">
                                                <input class="woocommerce-Button button" type="submit" value="Connexion" name="login">
                                            </p>
                                        </form>
                                        <!-- .woocommerce-form-login -->
                                    </div>
                                    <!-- .col-1 -->
                                    <!-- .col-2 -->
                                </div>
                                <!-- .col2-set -->
                                <!-- .customer-login-form -->
                            </div>
                            <!-- .woocommerce -->
                        </div>
                        <!-- .entry-content -->
                    </div>
                    <!-- .hentry -->
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
        <!-- .row -->
    </div>
    <!-- .col-full -->
</div>