<div id="content" class="site-content" tabindex="-1">
    <div class="col-full">
        <div class="row">
            <nav class="woocommerce-breadcrumb">
                <a href="<?php echo base_url(); ?>">Accueil</a>
                <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                Pratiques
            </nav>
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <section class="section-products-with-image">
                        <div class="landing-v2-page-header">
                            <h2 class="section-title">Voilà tout les bonne pratiques</h2>
                            <div class="text-block">Dans cette rubrique, nous avons regroupé pour vous les tutoriels les plus visionnés en ce qui concerne le dépannage, la réparation, l’installation et la maintenance des appareils électroménagers, des chauffages et des climatiseurs.</div>
                            <div class="text-block"><strong>Attention</strong> : Avant toute manipulation, veillez à ce que votre appareil soit débranché ou que le courant électrique soit coupé.</div>
                        </div>
                    </section>
                    <div class="quick-scroll">
                        <a class="quick-scroll-icon" href="javascript:void(0)">
                            <i class="tm tm-arrow-down"></i>
                        </a>
                    </div>
                    <?php
                    if (count($pratiques)) {
                        $nbr = 0;
                        foreach ($pratiques as $practice) {
                            $nbr++; ?>
                            <section class="section-products-with-image">
                                <header class="section-header">
                                    <div class="col-full">
                                        <div class="row">
                                            <div class="product-info">
                                                <h2 class="section-title"><?php echo $practice->title; ?></h2>
                                                <div class="data-author">
                                                    <time class="entry-date published"><?php echo date("d/M/Y", strtotime($practice->created_at)); ?></time>
                                                    <span class="author">
                                                    <a rel="author" title="Admin" href="javascript:void(0)"> - Admin</a>
                                                </span>
                                                </div>
                                                <div class="description"><?php echo $practice->description; ?></div>
                                            </div>
                                            <div class="product-image">
                                                <video width="406" height="300" controls>
                                                    <source src="<?php echo base_url() . $practice->banner; ?>" type="video/mp4"/>
                                                    Ici l'alternative à la vidéo : un lien de téléchargement, un message, etc.
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                </header>
                            </section>
                            <?php if ($nbr != count($pratiques)) { ?>
                                <div class="quick-scroll">
                                    <a class="quick-scroll-icon" href="javascript:void(0)">
                                        <i class="tm tm-arrow-down"></i>
                                    </a>
                                </div>
                            <?php }
                        }
                    } else {
                        ?>
                        <div class="liste-not-found">
                            <p>Aucune pratique dans la liste</p>
                        </div>
                    <?php } ?>
                    <!-- .section-products-with-image -->
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
        <!-- .row -->
    </div>
    <!-- .col-full -->
</div>