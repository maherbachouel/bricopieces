<div id="content" class="site-content page-template-default">
                <div class="col-full">
                    <div class="row">
                        <nav class="woocommerce-breadcrumb">
                            <a href="<?php echo base_url(); ?>">Accueil</a>
                            <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                            À propos
                        </nav>
                        <!-- .woocommerce-breadcrumb -->
                        <div id="primary" class="content-area">
                            <main id="main" class="site-main">
                                <div class="type-page hentry">
                                    <header class="entry-header">
                                        <div class="page-featured-image">
                                            <img width="1920" height="1391" alt="" class="attachment-full size-full wp-post-image" src="assets/images/products/about-header.jpg">
                                        </div>
                                        <!-- .page-featured-image -->
                                        <div class="page-header-caption">
                                            <h1 class="entry-title" style="color: #0063d1;">À PROPOS</h1>
                                            <!--p class="entry-subtitle">Passion may be a friendly or eager interest in or admiration for a proposal,
                                                <br> cause, discovery, or activity or love to a feeling of unusual excitement. </p-->
                                        </div>
                                        <!-- .page-header-caption -->
                                    </header>
                                    <!-- .entry-header -->
                                    <div class="entry-content">
                                        <div class="about-features row">
                                            <div class="col-md-12">
                                                <div class="text-block">
                                                    <p>Nous réparons toutes les marques réputées d'appareils ménagers et commerciaux, ainsi que fournissons, entretenons et installons tous les types de systèmes de climatisation. </p>
                                                    <p>En raison de notre niveau de service supérieur, beaucoup de nos clients nous considèrent comme leur fournisseur préféré de <strong>vente</strong> de <a class="redirect" href="<?php echo base_url().'services/electro-menager'?>">services électroménager</a>. Nous livrons notre travail avec intégrité et fierté et cela se voit.</p>
                                                    <p>Appelez-nous dès aujourd'hui pour planifier votre <a class="redirect" href="<?php echo base_url().'services/climatisation'?>">service de climatisation</a>, de chauffage ou de réparation d'appareils. Nous nous engageons à fournir un <strong>service rapide</strong> pour répondre à vos besoins immédiatement. </p>
                                                    <h2 class="align-top">Pourquoi nous choisir ?</h2>
													<ul style="list-style:none;">
													<li>Nos techniciens sont experts en réparation d'appareils chaud et <a class="redirect" href="<?php echo base_url().'services/froid'?>">froid</a> et en réfrigération.</li>
													<li>Les appareils sont minutieusement testés au sein de notre atelier.</li>
													<li>Axé sur la prestation d'un service de qualité professionnelle.</li>
													<li>Nous livrons notre travail avec intégrité et fierté et cela se voit.</li>
													<li>Un service amical, rapide et fiable.</li>
													</ul>
                                                </div>
                                                <!-- .text_block -->
                                            </div>
                                            <!-- .col -->
                                        </div>
                                        <!-- .about-features -->
                                        <!-- .team-member-wrapper -->
                                    </div>
                                    <!-- .entry-content -->
                                </div>
                                <!-- .hentry -->
                            </main>
                            <!-- #main -->
                        </div>
                        <!-- #primary -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .col-full -->
            </div>