<div class="page-title">
    <div class="title-env">
        <h1 class="title">Modifier une page</h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa-home"></i>Tableau de bord</a>
            </li>
            <li class="active ms-hover">
                <strong>Modifier une page</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <form role="form" enctype="multipart/form-data" method="post" class="form-horizontal" action="<?php echo base_url(); ?>admin/page/edit/<?php echo $page->id; ?>">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Titre</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" value="<?php echo $page->meta_title; ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Meta description</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="meta_description" value="<?php echo $page->meta_description; ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Slug(name)</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control"  name="slug" value="<?php echo $page->slug; ?>" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-grou text-right">
                <button class="btn btn-success btn-icon">
                    <i class="fa-check"></i>
                    <span>Enregistrer</span>
                </button>
            </div>
        </form>
    </div>
</div>