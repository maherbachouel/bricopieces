<div class="page-title">
    <div class="title-env">
        <h1 class="title">Tableau de bord</h1>
        <p class="description"></p>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="xe-widget xe-vertical-counter xe-vertical-counter-yellow" data-count=".num" data-from="0" data-to="<?php echo count($articles); ?>" data-decimal="," data-suffix="" data-duration="2.5">
            <div class="xe-icon">
                <i class="linecons-doc"></i>
            </div>
            <div class="xe-label">
                <strong class="num"><?php echo count($articles); ?></strong>
                <span>Articles</span>
            </div>
        </div>
    </div>
    <div class="col-sm-4">

        <div class="xe-widget xe-vertical-counter xe-vertical-counter-danger" data-count=".num" data-from="0" data-to="<?php echo count($produits); ?>" data-decimal="," data-suffix="" data-duration="3">
            <div class="xe-icon">
                <i class="linecons-truck"></i>
            </div>

            <div class="xe-label">
                <strong class="num"><?php echo count($produits); ?></strong>
                <span>Produits</span>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="xe-widget xe-vertical-counter xe-vertical-counter-white" data-count=".num" data-from="0" data-to="<?php echo count($pratique); ?>" data-duration="4">
            <div class="xe-icon">
                <i class="linecons-desktop"></i>
            </div>

            <div class="xe-label">
                <strong class="num"><?php echo count($pratique); ?></strong>
                <span>Pratiques</span>
            </div>
        </div>

    </div>
</div>