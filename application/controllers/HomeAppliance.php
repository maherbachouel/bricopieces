<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeAppliance extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('category_model', 'category');
    }
    public function index()
    {
        $data = array(
            'activePage'=>'electromenager',
            'filter'=>'',
            'categories' => $this->category->read('*'),
        );
        $view = $this->load->view('common/header', $data, TRUE);
        $view .= $this->load->view('service/electro-menager', $data, TRUE);
        $view .= $this->load->view('common/footer', $data, TRUE);
        echo $view;
    }
}
