<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pratiques extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pratique_model', 'pratique');
        $this->load->model('category_model', 'category');
    }

    public function index()
    {
        $data = array(
            'activePage' => 'pratiques',
            'filter' => '',
            'categories' => $this->category->read('*'),
            'pratiques' => $this->pratique->getAllPratique(),
        );
        $view = $this->load->view('common/header', $data, TRUE);
        $view .= $this->load->view('pratique/pratique', $data, TRUE);
        $view .= $this->load->view('common/footer', $data, TRUE);
        echo $view;
    }

}
