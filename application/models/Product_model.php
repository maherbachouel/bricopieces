<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_Model extends MY_Model
{
    protected $table = 'product';

    public function getRecentProduct(){
        return $this->db
            ->select('product.name as product_name')
            ->select('product.status as product_status')
            ->select('product.description as product_description')
            ->select('product.banner as product_banner')
            ->select('product.meta_description as product_meta_description')
            ->select('product.permalink as product_permalink')
            ->select('product.id as product_id')
            ->select('product.prix as product_prix')
            ->select('category.name as category_name')
            ->select('category.permalink as category_permalink')
            ->select('category.meta_description as category_meta_description')
            ->select('category.id as category_id')
            ->from($this->table)
            ->join('category', 'product.category_id=category.id', 'left')
            ->where('product.status', 1)
			->order_by('rand()')
            ->limit(7,0)
            ->get()
            ->result();
    }
    public function AddProduct($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function getProductById($id)
    {
        return $this->db->select('*')
            ->from($this->table)
            ->where('id', $id)
            ->get()
            ->result();
    }

    public function EditProduct($data, $id)
    {
        return $this->db->where('id', $id)
            ->update($this->table, $data);
    }


    public function getProducts()
    {
        return $this->db
            ->select('product.name as product_name')
            ->select('product.status as product_status')
            ->select('product.description as product_description')
            ->select('product.banner as product_banner')
            ->select('product.meta_description as product_meta_description')
            ->select('product.permalink as product_permalink')
            ->select('product.id as product_id')
            ->select('product.prix as product_prix')
            ->select('category.name as category_name')
            ->select('category.permalink as category_permalink')
            ->select('category.meta_description as category_meta_description')
            ->select('category.id as category_id')
            ->from($this->table)
            ->join('category', 'product.category_id=category.id', 'left')
            ->order_by('product.created_at', 'desc')
            ->get()
            ->result();
    }

    public function getAllProduct()
    {
        return $this->db
            ->select('product.name as product_name')
            ->select('product.status as product_status')
            ->select('product.description as product_description')
            ->select('product.banner as product_banner')
            ->select('product.meta_description as product_meta_description')
            ->select('product.permalink as product_permalink')
            ->select('product.id as product_id')
            ->select('product.prix as product_prix')
            ->select('category.name as category_name')
            ->select('category.permalink as category_permalink')
            ->select('category.meta_description as category_meta_description')
            ->select('category.id as category_id')
            ->from($this->table)
            ->join('category', 'product.category_id=category.id', 'left')
            ->where('product.status', 1)
            ->order_by('product.created_at', 'desc')
            ->get()
            ->result();
    }

    public function getProductByPermalink($permalink)
    {
        return $this->db
            ->select('product.name as product_name')
            ->select('product.description as product_description')
            ->select('product.banner as product_banner')
            ->select('product.meta_description as product_meta_description')
            ->select('product.permalink as product_permalink')
            ->select('product.id as product_id')
            ->select('product.prix as product_prix')
            ->select('product.banner')
            ->select('product.created_at as product_created_at')
            ->select('category.name as category_name')
            ->select('category.permalink as category_permalink')
            ->select('category.meta_description as category_meta_description')
            ->select('category.id as category_id')
            ->from($this->table)
            ->join('category', 'product.category_id=category.id', 'left')
            ->where('product.permalink', $permalink)
            ->where('product.status', 1)
            ->get()
            ->result();
    }

    public function getProductByCatPermalink($permalink)
    {
        return $this->db
            ->select('product.name as product_name')
            ->select('product.description as product_description')
            ->select('product.banner as product_banner')
            ->select('product.meta_description as product_meta_description')
            ->select('product.permalink as product_permalink')
            ->select('product.id as product_id')
            ->select('product.prix as product_prix')
            ->select('category.name as category_name')
            ->select('category.permalink as category_permalink')
            ->select('category.meta_description as category_meta_description')
            ->select('category.id as category_id')
            ->from($this->table)
            ->join('category', 'product.category_id=category.id', 'left')
            ->where('category.permalink', $permalink)
            ->where('product.status', 1)
            ->order_by('product.created_at', 'desc')
            ->get()
            ->result();
    }

}