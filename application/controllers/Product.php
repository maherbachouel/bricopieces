<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('category_model', 'category');
        $this->load->model('product_model', 'product');
    }

    public function index($permalinkcat = null)
    {
        $data = array(
            'activePage' => 'produits',
            'filter' => 'hidden',
            'categories' => $this->category->read('*'),
            'meta' => array()

        );
        if ($permalinkcat) {
            $cat = $this->category->getCategoryByPermalink($permalinkcat);
            if (!count($cat)) {
                redirect(base_url() . 'erreur-404');
            }
            $data['permalink'] = $permalinkcat;
            $data['produits'] = $this->product->getProductByCatPermalink($permalinkcat);
            $data['meta']['meta_title'] = $this->category->getCategoryByPermalink($permalinkcat)[0]->name;
            $data['meta']['meta_description'] = $this->category->getCategoryByPermalink($permalinkcat)[0]->meta_description;
        } else {
            $data['produits'] = $this->product->getAllProduct();
            $data['permalink'] = '';
        }

        $view = $this->load->view('common/header', $data, TRUE);
        $view .= $this->load->view('produit/produit', $data, TRUE);
        $view .= $this->load->view('common/footer', $data, TRUE);
        echo $view;
    }

    public function produitDetails($catPermalink, $prodPermalink)
    {

        $cat = $this->category->getCategoryByPermalink($catPermalink);
        $produit = $this->product->getProductByPermalink($prodPermalink);
        if (!count($cat) || !count($produit)) {
            redirect(base_url() . 'erreur-404');
        }
        $data = array(
            'activePage' => 'produits',
            'filter' => '',
            'categories' => $this->category->read('*'),
            'product' => $produit[0],
            'ogImage' => base_url() . $produit[0]->banner,
        );
        $data['meta']['meta_title'] = $produit[0]->product_name;
        $data['meta']['meta_description'] = $produit[0]->product_meta_description;

        $view = $this->load->view('common/header', $data, TRUE);
        $view .= $this->load->view('produit/single-produit', $data, TRUE);
        $view .= $this->load->view('common/footer', $data, TRUE);
        echo $view;
    }

}
