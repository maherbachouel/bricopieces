<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_Controller.php');
class Dashboard extends Admin_Controller
{


    public function index()
    {
        $this->load->model('blog_model', 'blog');
        $this->load->model('product_model', 'product');
        $this->load->model('pratique_model', 'pratique');

        $data = array(
            "user" => $this->user,
            "activepage" => "dashboard",
            "articles" => $this->blog->read('*'),
            "produits" => $this->product->read('*'),
            "pratique" => $this->pratique->read('*'),
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/dashboard', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }
}
