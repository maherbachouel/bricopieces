<div class="page-title">
    <div class="title-env">
        <h1 class="title">Liste des pages</h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa-home"></i>Tableau de bord</a>
            </li>
            <li class="active ms-hover">
                <strong>Liste des pages</strong>
            </li>
        </ol>
    </div>
</div>
<?php if (count($pages)) { ?>
    <div class="row">
        <div class="col-sm-12">
            <!--Basic Setup -->
            <div class="panel panel-default">
                <div class="panel-body">
                    <table id="admindatatable" class="dataTable table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Slug</th>
                            <th>Titre</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($pages as $page) {
                            ?>
                            <tr>
                                <td><?php echo $page->id; ?></td>
                                <td><?php echo $page->slug; ?></td>
                                <td><?php echo $page->meta_title; ?></td>
                                <td>
                                    <a href="<?php echo base_url(); ?>admin/page/edit/<?php echo $page->id; ?>"
                                       data-id="<?php echo $page->id; ?>"><i
                                            class="icon-admin linecons-pencil"></i></a>
                                    <a href="#" class="confirm-modal" data-href="<?php echo base_url(); ?>admin/page/delete"
                                       data-id="<?php echo $page->id; ?>"><i
                                            class="icon-admin fa-times-circle-o"></i></a></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php } else {
    ?>
    <div class="no-data">
        <div class="no-data-icon">
            <i class="fa fa-exclamation"></i>
        </div>
        <p class="no-data-text">Aucun élément dans la liste</p>
        <a href="<?php echo base_url(); ?>admin/page/add" class="no-data-link">Ajouter une page</a>
    </div>
<?php } ?>