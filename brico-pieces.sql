-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 08 jan. 2019 à 17:06
-- Version du serveur :  10.1.36-MariaDB
-- Version de PHP :  7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `brico-pieces`
--

-- --------------------------------------------------------

--
-- Structure de la table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `banner` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `blog`
--

INSERT INTO `blog` (`id`, `title`, `description`, `banner`, `meta_description`, `permalink`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Les avantages de la maintenance du climatiseur', '<p>Lorsque la temp&eacute;rature augmente et que le mercure dans le thermom&egrave;tre augmente, les propri&eacute;taires locaux vont utiliser leurs climatiseurs &agrave; une fr&eacute;quence de plus en plus grande.</p>\r\n\r\n<p>Le moment est venu de faire v&eacute;rifier votre syst&egrave;me pour tout probl&egrave;me que vous pourriez rencontrer &agrave; l&#39;avenir, en particulier dans le cas de climatiseurs plus anciens qui sont utilis&eacute;s r&eacute;guli&egrave;rement depuis de nombreuses ann&eacute;es.</p>\r\n\r\n<p>Cela peut vous permettre d&#39;&eacute;conomiser de l&#39;argent de diff&eacute;rentes mani&egrave;res et d&#39;am&eacute;liorer le fonctionnement g&eacute;n&eacute;ral de votre climatiseur.</p>\r\n\r\n<h2><strong>&Ccedil;a commence avec le service</strong></h2>\r\n\r\n<p>Je recommande &agrave; tous les utilisateurs des climatiseurs de programmer une maintenance pour son &eacute;quipement chaque printemps. Un technicien qualifi&eacute; viendra chez vous et proc&eacute;dera &agrave; une mise au point de votre syst&egrave;me, con&ccedil;ue pour remplir deux fonctions bien sp&eacute;cifiques:</p>\r\n\r\n<p><em><strong>V&eacute;rifier les probl&egrave;mes potentiels</strong></em></p>\r\n\r\n<p>Les gros probl&egrave;mes commencent g&eacute;n&eacute;ralement par de petits probl&egrave;mes, et un professionnel peut d&eacute;tecter les signes indiquant que quelque chose ne va pas bien avant une panne. La session de maintenance permet au technicien de regarder votre climatiseur en profondeur et de planifier une session de r&eacute;paration si cela le n&eacute;cessite.</p>\r\n\r\n<p><em><strong>Traiter&nbsp;toutes les petites g&ecirc;nes</strong></em></p>\r\n\r\n<p>Tous les probl&egrave;mes li&eacute;s au climatiseur ne n&eacute;cessitent pas une session de r&eacute;paration de taureau &agrave; r&eacute;soudre. Des petites choses comme des boulons desserr&eacute;s et des filtres encrass&eacute;s peuvent r&eacute;duire l&rsquo;efficacit&eacute; de votre syst&egrave;me et engendrer des probl&egrave;mes bien plus graves sur toute la ligne.</p>\r\n\r\n<p>Une session de maintenance permet au technicien de s&rsquo;occuper de tous ces probl&egrave;mes: changer les filtres, resserrer les raccords desserr&eacute;s, lubrifier les pi&egrave;ces mobiles et s&rsquo;assurer autrement que tout est en forme de bateau.</p>\r\n\r\n<h2><strong>Quels sont les b&eacute;n&eacute;fices?</strong></h2>\r\n\r\n<p>Les avantages de l&rsquo;entretien de la climatisation sont subtils, mais ils peuvent faire toute la diff&eacute;rence l&agrave; o&ugrave; ils comptent le plus: votre portefeuille, plus pr&eacute;cis&eacute;ment.</p>\r\n\r\n<ul>\r\n	<li>Si des r&eacute;parations plus importantes sont n&eacute;cessaires, d&eacute;couvrir le probl&egrave;me vous donne maintenant le temps de les r&eacute;soudre correctement, avant que le temps ne devienne trop chaud. Cela contraste avec le fait de se d&eacute;mener face &agrave; une panne inattendue au milieu de l&rsquo;&eacute;t&eacute;.</li>\r\n	<li>Les r&eacute;parations sont &eacute;galement susceptibles de co&ucirc;ter moins cher, car r&eacute;soudre le probl&egrave;me &agrave; un stade pr&eacute;coce signifie que les dommages n&#39;ont pas eu autant de temps pour se propager.</li>\r\n	<li>R&eacute;soudre de petits probl&egrave;mes permet &agrave; votre syst&egrave;me de fonctionner plus efficacement, de gaspiller moins d&rsquo;&eacute;nergie et de mieux faire son travail. Cela peut vous faire &eacute;conomiser &eacute;norm&eacute;ment sur vos co&ucirc;ts mensuels au cours de l&rsquo;&eacute;t&eacute;. Effectuer des travaux d&rsquo;entretien signifie maintenant que vous pourrez profiter de ces &eacute;conomies d&egrave;s que la temp&eacute;rature commencera &agrave; monter.</li>\r\n</ul>\r\n\r\n<h2><strong>Conclusion</strong></h2>\r\n\r\n<p>Lorsqu&#39;ils sont appliqu&eacute;s r&eacute;guli&egrave;rement, au moins une fois par an, les travaux de maintenance peuvent aider &agrave; r&eacute;duire l&#39;usure que subit votre syst&egrave;me. Cela r&eacute;duit non seulement les chances et la fr&eacute;quence des pannes futures, mais peut &eacute;galement prolonger la vie de votre syst&egrave;me de plusieurs mois, voire de plusieurs ann&eacute;es et dans certains cas: vous aider &agrave; tirer le meilleur parti d&#39;un investissement tr&egrave;s co&ucirc;teux.</p>\r\n', 'uploads/blog/1546962702.jpg', 'Retrouvez les principaux avantages et inconvénients de la maintenance du climatiseur à connaitre avant d\'opter pour ce système.Consultez notre article !', 'les-avantages-de-la-maintenance-du-climatiseur', 1, '2019-01-08 09:56:07', '2019-01-08 16:51:42');

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `name`, `meta_description`, `permalink`, `created_at`, `updated_at`) VALUES
(5, 'Chauffe-eau solaire', 'Brico Pièces vous vend des pièces de rechange des chauffe eaux solaires,Intervention sur tous l’île de Djerba.', 'chauffe-eau-solaire', '2018-12-29 16:32:50', '2019-01-05 14:19:54'),
(6, 'Pieces chauffe-eau', 'Intervention sur tous l’île de Djerba, Vente des Pièces chauffe eaux .Vente des Pièces de rechange,détachées.', 'pieces-chauffe-eau', '2018-12-29 16:32:59', '2019-01-05 14:26:32'),
(7, 'Climatisation', 'Retrouvé tous les pièces et accessoires pour climatiseurs , Achat de toutes pièces détachées pour votre climatiseur, à prix bas à Djerba.', 'climatisation', '2018-12-29 16:33:08', '2019-01-03 17:49:31'),
(8, 'Four et micro-onde', 'Retrouvez dans cette catégorie l\'ensemble des pièces détachées et accessoires pour réparer votre Four micro-ondes. De nombreuses pièces disponibles .', 'four-et-micro-onde', '2018-12-29 16:33:18', '2019-01-03 17:49:35'),
(9, 'Machine laver automatique', 'Réparer votre machine laver avec nos pièces détachées et  retrouvez tous les produits d\'entretien et accessoires pour votre Machine à laver automatique.', 'machine-laver-automatique', '2019-01-03 13:43:58', '2019-01-03 17:49:43'),
(10, 'Machine à laver semi automatique', 'Machine à laver semi automatique, pièces de rechange à prix pas cher en Tunisie,Djerba.', 'machine-laver-semi-automatique', '2019-01-03 13:44:40', '2019-01-03 17:49:48'),
(11, 'Froid Commercial', ' Froid Commercial : Trouver vos pièces détachées,rechange à Djerba ', 'froid-commercial', '2019-01-03 13:44:52', '2019-01-03 17:49:53'),
(12, 'Froid Ménager', 'Vente de pièces détachées et accessoires pour l\'électroménager ,Toutes les pièces détachées pour froid ménager à Djerba (Brico Pièces) à prix pas chère.', 'froid-mnager', '2019-01-03 13:45:02', '2019-01-03 17:49:59'),
(13, 'Gaz', 'Vous trouvez une grande sélection de bouteille de gaz de climatisation dans Brico Pièces à Djerba, recharger le gaz de votre climatiseur .', 'gaz', '2019-01-03 13:45:17', '2019-01-03 17:50:04'),
(14, 'Outillages Frigoriste', 'Brico Pièces vous propose une gamme des pièces de rechange d\'outillage frigoriste de haute qualité sur l’île de Djerba.', 'outillages-frigoriste', '2019-01-03 13:45:34', '2019-01-03 17:50:09');

-- --------------------------------------------------------

--
-- Structure de la table `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `newsletter`
--

INSERT INTO `newsletter` (`id`, `email`) VALUES
(2, 'maher@maher.com');

-- --------------------------------------------------------

--
-- Structure de la table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `page`
--

INSERT INTO `page` (`id`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(5, 'accueil', 'Brico Pièces : Vente des pièces de rechange - Service Pro !', 'Brico Pièces est une entreprise de dépannage chauffage, froid,électroménager et climatisation sur tout l\'Île de Djerba. Service de vente et Réparation.', '2019-01-03 15:13:52', '2019-01-04 15:40:36'),
(6, 'apropos', 'Trouvez tous les informations nécessaires sur Brico Pièces', 'Si vous avoir plus d\'informations sur les services de Brico Pièces, ne paniquiez pas à consulter notre site ou nous appeler sur le (+216) 55 64 62 17', '2019-01-04 14:47:54', '2019-01-04 15:41:49'),
(7, 'produits', 'Brico Pièces : Consultez les différents produits offerts ', 'Produits à vendre par Brico Pièces: Consultez nos différents produits ... Service de vente. Prestation de qualité et Pro !', '2019-01-04 14:48:04', '2019-01-07 13:48:11'),
(8, 'services/climatisation', 'Service de climatisation à Djerba : Réparation, installation ', 'Climatisation pas cher à Djerba : Pour tous vos travaux de dépannage, réparation, pose et installation contactez Brico Pièces, Intervention en urgence.', '2019-01-04 14:48:13', '2019-01-08 11:55:46'),
(9, 'services/chauffage', 'Vente des pièces de rechange de chauffage : Réparation ', 'Réparation Chauffage Djerba : Service d\'installation, pose et dépannage et vente des services des pièces de rechange sur toute l\'Île de Djerba.', '2019-01-04 14:48:27', '2019-01-08 12:17:43'),
(10, 'services/electro-menager', 'Pièces de rechange des électroménagers Djerba : Vente & Réparation', 'Réparation et dépannage électroménagers à Djerba : Trouvez le meilleur artisan pas cher et de confiance. Intervention rapide .', '2019-01-04 14:48:38', '2019-01-08 12:17:49'),
(11, 'services/froid', 'Vente des pièces de rechange - Dépannage et Réparation', 'Brico Pièces vous propose des services pour les professionnels dans les domaines variés: Industrie, Métiers de bouche, Installateurs.', '2019-01-04 14:48:51', '2019-01-08 12:18:01'),
(12, 'pratiques', 'Trouvez les meilleurs démos explicatifs (Guide 2019)', 'Vous avez une interrogation sur certains astuces et technologies, alors n\'hésitez pas à consulter note page web. Nouvelles actualités 2019.', '2019-01-04 14:48:59', '2019-01-04 15:43:41'),
(13, 'blog', 'Trouvez les dernières actualités sur le bricolage', 'Blog Brico Pièces : Les nouvelles et les dernières actualités, des guides pratiques bien détaillés. Consultez le blog su Bicro Pièces.', '2019-01-04 14:49:06', '2019-01-04 15:44:10'),
(14, 'contact', 'Brico Pièces : Coordonnées et les informations professionnelles', 'Trouvez tous les coordonnés et les différentes informations professionnelles de Brico Pièces. Contactez-nous.', '2019-01-04 14:49:15', '2019-01-04 15:47:08'),
(15, 'connexion', 'Brico Pièces : Connexion à la partie admin', 'Connexion à la partie admin', '2019-01-04 14:49:38', '2019-01-04 14:55:35'),
(16, 'services', 'Tous nos service à Djerba : Climatisation, Réparation, installation,', 'Vous voudriez profiter des services électroménager, froid, chauffage, climatisation à Djerba ? Contactez Brico Pièces Service Pro ! ', '2019-01-08 11:27:24', '2019-01-08 11:28:19');

-- --------------------------------------------------------

--
-- Structure de la table `practice`
--

CREATE TABLE `practice` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `banner` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `prix` float NOT NULL,
  `status` int(11) NOT NULL,
  `banner` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`id`, `name`, `description`, `meta_description`, `permalink`, `prix`, `status`, `banner`, `created_at`, `updated_at`, `category_id`) VALUES
(35, 'Anode chauffe-eau solaire', '<p>L&#39;anode d&#39;un chauffe-eau sert &agrave; prot&eacute;ger la cuve de votre chauffe-eau de la corrosion.</p>\r\n\r\n<p>Elle se corrode &agrave; la place de votre cuve&nbsp;: en s&#39;oxydant, elle se d&eacute;truit petit &agrave; petit &agrave; la place des parois de la cuve.</p>\r\n\r\n<p>La dur&eacute;e de vie de votre chauffe-eau d&eacute;pend directement de la dur&eacute;e de vie de son anode.</p>\r\n', 'Vente d\'Anode chauffe-eau solaire à Djerba avec un prix bas , contactez nous !', 'anode-chauffe-eau-solaire', 0, 1, 'uploads/product/1546808563.jpg', '2019-01-06 22:02:43', '2019-01-07 09:14:27', 5),
(36, 'Résistance SOFTEN', '<p>Elles ne sont donc pas au contact direct de l&#39;eau, ce qui permet un remplacement relativement simple et rapide.<br />\r\nSur le chauffe-eau&nbsp;la r&eacute;sistance est ins&eacute;r&eacute;e dans un fourreau &eacute;maill&eacute; ou corps de chauffe et n&#39;est pas au contact direct de l&#39;eau. le corps de chauffe a l&#39;avantage de prot&eacute;ger la r&eacute;sistance des risques d&#39;entartrage.</p>\r\n\r\n<p>Ce syst&egrave;me &eacute;vite de vidanger votre cuve lors de l&#39;entretien et de la v&eacute;rification du chauffe-eau. V&eacute;rifiez r&eacute;guli&egrave;rement l&#39;&eacute;tat de l&#39;anode et de la r&eacute;sistance &eacute;lectrique, 1 fois par an si votre eau est particuli&egrave;rement calcaire ou acide.</p>\r\n', ' Découvrez notre gamme de résistance SOFTEN à Djerba, Choisissez une gamme des résistances adapté à vos besoins.', 'resistance-soften', 0, 1, 'uploads/product/1546808633.jpg', '2019-01-06 22:03:53', '2019-01-07 10:34:54', 5),
(37, 'Résistances DUROTHERM', '<p>Les &eacute;l&eacute;ments chauffants&nbsp;sont r&eacute;alis&eacute;s suivant plan, mod&egrave;le ou sp&eacute;cifications du client. La longueur d&eacute;velopp&eacute;e maxi est &eacute;gale &agrave; 6600 mm.</p>\r\n\r\n<p>L&rsquo;&eacute;l&eacute;ment chauffant, de diam&egrave;tre 6,5 &agrave; 16 mm, peut &ecirc;tre r&eacute;alis&eacute; en cuivre aciers inoxydables, incolloy ou inconel.</p>\r\n\r\n<p>La puissance surfacique est adapt&eacute;e aux exigences du process. Les &eacute;l&eacute;ments peuvent &ecirc;tre fabriqu&eacute;s &agrave; l&rsquo;unit&eacute; ou de s&eacute;rie.&nbsp;</p>\r\n', 'Trouvez le produit qui vous convient en quelques clics résistances DUROTHERM, Brico Pièces vous aide à trouvé votre pack.', 'resistances-durotherm', 0, 1, 'uploads/product/1546808794.jpg', '2019-01-06 22:06:34', '2019-01-07 10:31:46', 5),
(38, 'Résistances', '<p>Le choix de la puissance de la r&eacute;sistance est important, pr&eacute;f&eacute;rez le 2200 &agrave; 2500 W.Une puissance inf&eacute;rieure prendra plus de temps pour chauffer et une puissance trop forte risque de faire sauter votre disjoncteur si il est utilis&eacute; en m&ecirc;me temps que le four ou autre appareil.</p>\r\n', 'Vente des résistances sur mesure et  pas cher. Envie de faire nettoyer votre chauffe-eau solaire? ', 'resistances', 0, 1, 'uploads/product/1546808882.jpg', '2019-01-06 22:08:02', '2019-01-07 10:32:58', 5),
(39, 'Bloc chauffe-eau', '<p>Le corps de chauffe assure la production de chaleur, mais peu &agrave; peu il s&#39;encrasse et son rendement diminue.</p>\r\n\r\n<p>Un entretien annuel pour&nbsp;nettoyer le corps de chauffe d&#39;un chauffe&nbsp;eau est donc n&eacute;cessaire.</p>\r\n', 'Découvrez nos produits de chauffe-eau :  Choisissez vos produits et notre boutique Brico Pièces vous aide à faire l’installation à Djerba.', 'bloc-chauffe-eau', 0, 1, 'uploads/product/1546809025.jpg', '2019-01-06 22:10:25', '2019-01-07 10:35:08', 6),
(40, 'Carte chauffe-eau', '<p>Le thermostat est un accessoire tr&egrave;s pratique qui permet de&nbsp;r&eacute;gler&nbsp;et donc de&nbsp;limiter la temp&eacute;rature de l&#39;eau de votre chauffe-eau.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'Découvrez notre Pack carte chauffe-eau. Brico Pièces vous propose une multitude de services,produits pour répondre à chacun de vos besoins à Djerba. ', 'carte-chauffe-eau', 0, 1, 'uploads/product/1546809091.jpg', '2019-01-06 22:11:31', '2019-01-07 10:34:40', 6),
(41, 'Porte pile', '<p>Cette bo&icirc;te de batterie durable conserve ses propri&eacute;t&eacute;s d&#39;impact jusqu&#39;&agrave; -20F et est &eacute;galement r&eacute;sistante au UV, p&eacute;trole et a l&rsquo;essence.</p>\r\n\r\n<p>Boite pour piles&nbsp;&nbsp;permet de positionner 2 blocs de batteries&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'Brico Pièces: Petits prix sur les produits Porte pile , Pièces chauffe-eau à Djerba', 'porte-pile', 0, 1, 'uploads/product/1546809123.jpg', '2019-01-06 22:12:03', '2019-01-07 09:40:22', 6),
(42, 'Boite climatisation encastré', '<p>FREE BOX avec &eacute;chappement r&eacute;versible</p>\r\n\r\n<p>&bull; sortie de d&eacute;charge r&eacute;versible RX ou LH, horizontale et verticale;<br />\r\n&bull; base de collecte en pente;<br />\r\n&bull; Raccordement pour raccord de tuyau &Oslash; 16/18 et 18/20;<br />\r\n&bull; Gabarit en carton pour la protection lors de l&#39;installation;<br />\r\n&bull; Livr&eacute; dans un seul emballage;<br />\r\n&bull; Complet avec joints toriques et vis</p>\r\n', 'Vente des pièces de rechange , Trouvez tous les Boite climatisation encastré à Djerba.', 'boite-climatisation-encastre', 0, 1, 'uploads/product/1546898204.jpg', '2019-01-07 22:56:44', '2019-01-08 14:51:30', 7),
(43, 'Carte climatiseur', '', ' Retrouvez toutes les pièces détachées de votre climatiseur parmi nos pièces. Brico pièce vend tous les pièces de rechange à Djerba.', 'carte-climatiseur', 0, 1, 'uploads/product/1546898280.jpg', '2019-01-07 22:58:00', '2019-01-08 14:48:27', 7),
(44, 'Carte universelle pour climatiseur', '<p>Carte Universelle pour climatiseur chaud/froid de type ordinaires on/off.</p>\r\n\r\n<p>Evite la ventilation &agrave; froid en mode de chauffe, pour votre confort.</p>\r\n\r\n<p>Pr&eacute;sence d&#39;une sortie d&eacute;di&eacute;e pour le ventilateur ext&eacute;rieur d&#39;usage optionnelle afin d&#39;obtenir un bon d&eacute;givrage.</p>\r\n\r\n<p>Note : cette carte peut &ecirc;tre utilis&eacute;e pour un climatiseur &quot;froid seul&quot;. Il suffira de ne pas raccorder les connections pour &eacute;lectrovanne 4 voies.</p>\r\n', 'Venez découvrir notre sélection de produits carte électronique climatiseur au meilleur prix sur Brico Pièces à Djerba . ', 'carte-universelle', 0, 1, 'uploads/product/1546898310.jpg', '2019-01-07 22:58:30', '2019-01-08 14:41:59', 7),
(45, 'Commande climatiseur (2)', '<p>Comme toute t&eacute;l&eacute;commande, la t&eacute;l&eacute;commande d&rsquo;un climatiseur sert &agrave; le&nbsp;<strong>commander &agrave; distance</strong>&nbsp;et plus pr&eacute;cis&eacute;ment, &agrave; choisir les diff&eacute;rents r&eacute;glages de fonctionnement.</p>\r\n\r\n<p>C&rsquo;est gr&acirc;ce &agrave; la t&eacute;l&eacute;commande que vous pourrez ajuster la temp&eacute;rature &agrave; vos habitudes de vie et que vous pourrez &eacute;teindre et activer votre climatiseur.&nbsp;</p>\r\n', 'Trouvez le produit qui vous convient en quelques clics.Commande climatiseur , accessoires de votre climatiseur, Brico Pièces vous aide.', 'commande-climatiseur-2', 0, 1, 'uploads/product/1546898356.jpg', '2019-01-07 22:59:16', '2019-01-08 14:36:54', 7),
(46, 'Commande climatiseur (1)', '<p>La t&eacute;l&eacute;commande de climatiseur, permet d&#39;utiliser les fonctions suivantes&nbsp;:</p>\r\n\r\n<ul>\r\n	<li>R&eacute;gler la vitesse de la ventilation&nbsp;: automatique, petite, moyenne ou grande vitesse</li>\r\n	<li>Affichage de la temp&eacute;rature int&eacute;rieure.</li>\r\n	<li>Programmation de la dur&eacute;e de fonctionnement.</li>\r\n	<li>Gestion du balayage d&#39;air&nbsp;: automatique ou fixe.</li>\r\n	<li>R&eacute;gler la vitesse de la ventilation&nbsp;: automatique, petite, moyenne ou grande vitesse</li>\r\n</ul>\r\n', 'Commande climatiseur , Régler la vitesse de la ventilation, Gestion du balayage d\'air , tous vos pièces de rechange à Djerba.', 'commande-climatiseur-1', 0, 1, 'uploads/product/1546898374.jpg', '2019-01-07 22:59:34', '2019-01-08 14:32:29', 7),
(47, 'Commande climatiseur', '<p>De mani&egrave;re plus concise, la t&eacute;l&eacute;commande de climatiseur, selon les mod&egrave;les, permet d&#39;utiliser les fonctions suivantes&nbsp;:</p>\r\n\r\n<ul>\r\n	<li>Marche/arr&ecirc;t.</li>\r\n	<li>R&eacute;gler la consigne de temp&eacute;rature d&eacute;sir&eacute;e.</li>\r\n	<li>Verrouillage de la t&eacute;l&eacute;commande.</li>\r\n	<li>Choisir le mode de fonctionnement&nbsp;: automatique, froid, chaud, ventilation, d&eacute;shumidification d&#39;air, turbo</li>\r\n	<li>Gestion du balayage d&#39;air&nbsp;: automatique ou fixe.</li>\r\n</ul>\r\n', 'Vente de télécommande de climatiseur à Brico pièces , Djerba Elmay  ', 'commande-climatiseur', 0, 1, 'uploads/product/1546898390.jpg', '2019-01-07 22:59:50', '2019-01-08 14:32:31', 7),
(48, 'Compresseur climatiseur', '<p>Le&nbsp;<strong>compresseur de climatisation</strong>&nbsp;est l&#39;&eacute;l&eacute;ment principal du syst&egrave;me.</p>\r\n\r\n<p>Son r&ocirc;le est de compresser le gaz de climatisation de mani&egrave;re &agrave; l&#39;&eacute;chauffer et le faire monter en pression.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'Le compresseur de climatisation est l\'élément principal du système, appelez sur (+216) 55 64 62 17.', 'compresseur-climatiseur', 0, 1, 'uploads/product/1546898510.jpg', '2019-01-07 23:01:50', '2019-01-08 14:28:13', 7),
(49, 'Condensateur', '<p>Condensateur de climatiseur</p>\r\n', 'Tous les catégories des Condensateurs de climatiseur à Djerba - Tunisie à prix pas cher.', 'condensateur', 0, 1, 'uploads/product/1546898530.jpg', '2019-01-07 23:02:10', '2019-01-08 14:26:52', 7),
(50, 'Condensateurs', '<p>Le condenseur va faire passer le fluide frigorig&egrave;ne de l&#39;&eacute;tat gazeux &agrave; l&#39;&eacute;tat liquide (condensation).</p>\r\n\r\n<p>Le condenseur d&#39;un climatiseur ou d&#39;une pompe &agrave; chaleur est la partie du circuit dans laquelle le fluide frigorig&egrave;ne va perdre sa chaleur au profit d&#39;un fluide caloporteur ou de l&#39;air dans le cas d&#39;un circuit &agrave; d&eacute;tente directe.</p>\r\n\r\n<p>Un condenseur se pr&eacute;sente la plupart du temps sous forme d&#39;un &eacute;changeur muni d&#39;une multitude d&#39;ailettes destin&eacute;es &agrave; augmenter la surface d&#39;&eacute;change thermique.</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n', 'Brico Pièces : Vente et installation des Condensateurs de climatiseur à Djerba.', 'condensateurs', 0, 1, 'uploads/product/1546898552.jpg', '2019-01-07 23:02:32', '2019-01-08 14:25:48', 7),
(51, 'Cuivre frigorifique calorifugé', '<p>Tube de cuivre recuit d&rsquo;&eacute;paisseur 0,8 ou 1 mm, conforme &agrave; la norme EN 12735-1.</p>\r\n\r\n<p>Le tube cuivre&nbsp; b&eacute;n&eacute;ficie d&rsquo;un &eacute;tat de surface interne de tr&egrave;s haute qualit&eacute;, brillant et exempt de poussi&egrave;re et d&rsquo;humidit&eacute;, r&eacute;pondant ainsi aux exigences les&nbsp;plus s&eacute;v&egrave;res dans le domaine du transport des fluides frigorig&egrave;nes.</p>\r\n\r\n<p>Cet &eacute;tat de propret&eacute; est maintenu jusqu&rsquo;au chantier gr&acirc;ce &agrave; des capes pos&eacute;es aux extr&eacute;mit&eacute;s du tube.</p>\r\n\r\n<p>Gaine isolante en mousse de poly&eacute;thyl&egrave;ne &agrave; cellules ferm&eacute;es rev&ecirc;tue d&rsquo;un film de protection de couleur beige, pare-vapeur.</p>\r\n\r\n<p>Distribution des fluides frigorig&egrave;nes pour la climatisation, le conditionnement d&rsquo;air et la r&eacute;frig&eacute;ration.</p>\r\n', 'Vous pièces de rechange à Djerba , Cuivre frigorifique calorifugé, Installation et remplacement à un prix imbattable.', 'cuivre-frigorifique-calorifuge', 0, 1, 'uploads/product/1546898605.jpg', '2019-01-07 23:03:25', '2019-01-08 14:23:13', 7),
(52, 'à verifier gorge de condensat', '<p>gorge de condensat</p>\r\n', 'gorge de condensat', 'verifier-gorge-de-condensat', 0, 1, 'uploads/product/1546898708.jpg', '2019-01-07 23:05:08', '2019-01-07 23:05:08', 7),
(53, 'Goulotte spécifique', '<p>Ce syst&egrave;me de goulotte , assorti d&rsquo;une gamme compl&egrave;te d&rsquo;accessoires, lui permet de s&#39;int&eacute;grer partout de mani&egrave;re esth&eacute;tique, en neuf comme en r&eacute;novation.</p>\r\n', 'Brico Pièces : goulotte spécifique assorti d’une gamme complète d’accessoires, trouvez vous tous les accessoires de climatiseur à Djerba.', 'goulotte-specifique', 0, 1, 'uploads/product/1546898752.jpg', '2019-01-07 23:05:52', '2019-01-08 14:14:58', 7),
(54, 'Hélice climatiseurs unités extérieurs', '<p>H&eacute;lice climatiseurs unit&eacute;s ext&eacute;rieurs&nbsp;misez sur l&#39;efficacit&eacute; de cette H&eacute;lice de ventilateur h&eacute;lico&icirc;de adapt&eacute;e aux professionnels pour de multiples applications de ventilation.</p>\r\n\r\n<p>Elle dispose de 3 p&acirc;les rondes pour un un mode de fonctionnement aspirant.</p>\r\n', 'Installation et remplacement hélice climatiseurs unités extérieurs à un prix imbattable.', 'helice-climatiseurs-unites-extrieurs', 0, 1, 'uploads/product/1546898809.jpg', '2019-01-07 23:06:49', '2019-01-08 14:01:42', 7),
(55, 'Turbine ventilateur unité intérieure', '<p>Le ventilateur tangentiel se pr&eacute;sente sous la forme d&#39;une turbine, qui ressemble &agrave; une &laquo;&nbsp;roue &agrave; aube&nbsp;&raquo; ou une &laquo;&nbsp;roue de hamster&nbsp;&raquo; plut&ocirc;t longue, contrairement aux ventilateurs &laquo;&nbsp;classiques&nbsp;&raquo;, qui ont la forme d&#39;une &laquo;&nbsp;h&eacute;lice&nbsp;&raquo;.</p>\r\n\r\n<p>Cette pi&egrave;ce d&eacute;tach&eacute;e Turbine ventilateur unit&eacute; int&eacute;rieure s&rsquo;adapte sur plusieurs appareils.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'Trouvez tous les Turbine ventilateur unité intérieure à Djerba de vos climatiseurs.', 'turbine-ventilateur-unite-interieure', 0, 1, 'uploads/product/1546898873.jpg', '2019-01-07 23:07:53', '2019-01-08 13:58:06', 7),
(56, 'Moto-ventilateur unité extérieur climatiseur', '<p>Moto ventilateur unit&eacute; ext&eacute;rieur climatiseur</p>\r\n', 'Trouvez le produit qui vous convient en quelques clics. Moto ventilateur unité extérieur climatiseur, Brico Pièces vous aide à l’installation de votre pack.', 'moto-ventilateur-unite-exterieur-climatiseur', 0, 1, 'uploads/product/1546898913.jpg', '2019-01-07 23:08:33', '2019-01-08 13:46:49', 7),
(57, 'Moto-ventilateur unité intérieur climatiseurs', '<p>Le moteur permet de faire tourner l&#39;h&eacute;lice du ventilateur&nbsp;et ainsi de refroidir le circuit r&eacute;frig&eacute;rant.</p>\r\n\r\n<p>Ce syst&egrave;me permet d&#39;augmenter la puissance de refroidissement de votre climatiseur tout en r&eacute;duisant le bruit et la consommation d&rsquo;&eacute;lectricit&eacute;.</p>\r\n', 'Profitez de nos packs de climatisation à petits prix ! Brico Pièces vous propose le nouveau moto-ventilateur unité intérieurs climatiseurs .', 'moto-ventilateur-unite-interieur-climatiseurs', 0, 1, 'uploads/product/1546898950.jpg', '2019-01-07 23:09:10', '2019-01-08 13:41:09', 7),
(58, 'Robinet climatisation auto', '<p>Le syst&egrave;me de climatisation automobile est constitu&eacute; de diff&eacute;rents &eacute;l&eacute;ments. Compresseur, condenseur, &eacute;vaporateur&hellip; : chacun contribue au bon fonctionnement global de la climatisation auto.</p>\r\n', 'Découvrez notre Pack prise de mesures pour les robinet climatisation auto.', 'robinet-climatisation-auto', 0, 1, 'uploads/product/1546899019.jpg', '2019-01-07 23:10:19', '2019-01-08 13:33:48', 7),
(59, 'Support de toiture climatiseur', '<p>Support toiture pour groupe de climatisation .<br />\r\n<br />\r\nId&eacute;al pour la mise en place simple et rapide d&#39;un groupe ext&eacute;rieur de climatisation.</p>\r\n', 'Brico Pièces : Support de toiture  climatiseur', 'support-de-toiture-climatiseur', 0, 1, 'uploads/product/1546899040.jpg', '2019-01-07 23:10:40', '2019-01-08 11:14:36', 7),
(61, 'Support de toiture climatiseurs', '<p>Support toiture pour groupe de climatisation .<br />\r\n<br />\r\nId&eacute;al pour la mise en place simple et rapide d&#39;un groupe ext&eacute;rieur de climatisation.</p>\r\n', 'Envie de faire installer un climatiseur  ? Brico Pièces vend des supports de toiture climatiseurs à Djerba', 'support-de-toiture-climatiseurs', 0, 1, 'uploads/product/1546899083.jpg', '2019-01-07 23:11:23', '2019-01-08 11:13:24', 7),
(62, 'Transformateur climatiseur', '<p>Les transformateurs sont des convertisseurs d&rsquo;&eacute;nergie&nbsp;&eacute;lectrique, ils permettent d&rsquo;adapter un courant &eacute;lectrique en fonction de l&rsquo;utilisation.</p>\r\n\r\n<p>Les transformateurs sont partout autour de nous, qu&rsquo;ils soient de tr&egrave;s grosses puissances somme ceux utilis&eacute;s dans le r&eacute;seau de distribution &eacute;lectrique ou de tr&egrave;s petites comme les transformateurs monophas&eacute;s des appareils que l&rsquo;on utilise tous les jours, chargeur pour ordinateur ou t&eacute;l&eacute;phone portable, rasoir &eacute;lectrique, etc.</p>\r\n\r\n<p>Notons qu&rsquo;un transformateur modifie suivant le cas, la tension (Volt) et l&rsquo;intensit&eacute; (Amp&egrave;re) d&rsquo;un courant &eacute;lectrique sans toutefois en changer la&nbsp;fr&eacute;quence&nbsp;(50 Hz en France).</p>\r\n', 'Découvrez nos produits de transformateur, Brico Pièces vous aide à faire l’installation. ', 'transformateur-climatiseur', 0, 1, 'uploads/product/1546899104.jpg', '2019-01-07 23:11:44', '2019-01-08 11:08:02', 7);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `api_key` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `firstname`, `lastname`, `username`, `api_key`, `password`, `created_at`, `updated_at`, `role`) VALUES
(4, 'contact@brico-pieces.com', 'Brico', 'Pièces', 'brico-pieces', 'qKjp4IqCf5UV8DpVPUq1xRCEVm5U1MNGVe99M8DFmbgRe9IEVmFKfIpJeDtVe5YqKjpPUq41MNG1xRC9MFmbgRJeDtqCfIFKfe5Y', 'e4a82bff8d6c0cda382c7a6714becd1a6fe1b3f56dcd5ac09ee511af689951a9ffbee60e861c2eb000b476568e7aca702a5e5bde652b5a572969104a6b4ecb88iGqKz8vccLjhEhIVA+UE8d9gx5QUBYwr3aJHZzeIVxTmXLF8sHnVVs6DEjahr0c9', '2018-12-28 15:07:56', '2018-12-28 15:07:56', 'SUPER_ADMIN');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `practice`
--
ALTER TABLE `practice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `practice`
--
ALTER TABLE `practice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
