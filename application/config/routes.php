<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['apropos'] = 'about';
$route['services'] = 'services';
$route['services/climatisation'] = 'airconditionning';
$route['services/electro-menager'] = 'homeappliance';
$route['services/froid'] = 'cold';
$route['services/chauffage'] = 'Heating';
$route['pratiques'] = 'pratiques';
$route['blog'] = 'blog';
$route['blog/(:any)'] = 'blog/blogdetails/$1';
$route['contact'] = 'contact';
$route['produits/(:any)/(:any)'] = 'product/produitdetails/$1/$2';
$route['produits/(:any)'] = 'product/index/$1';
$route['produits'] = 'product';
$route['connexion'] = 'connexion';
$route['admin'] = 'admin/dashboard';

$route['climatisation'] = 'error_404';
$route['froid'] = 'error_404';
$route['chauffage'] = 'error_404';
$route['electro-menager'] = 'error_404';

$route['erreur-404'] = 'error_404/error';
$route['404_override'] = 'error_404';
$route['404-error'] = 'error_404';
$route['translate_uri_dashes'] = FALSE;
