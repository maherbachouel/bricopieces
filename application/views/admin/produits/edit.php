<div class="page-title">
    <div class="title-env">
        <h1 class="title">Modifier un produit</h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa-home"></i>Tableau de bord</a>
            </li>
            <li class="active ms-hover">
                <strong>Modifier un produit</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <form role="form" enctype="multipart/form-data" method="post" class="form-horizontal" action="<?php echo base_url(); ?>admin/produits/edit/<?php echo $produit->id; ?>">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control keyup-meta" name="name" value="<?php echo $produit->name; ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Permalink</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control permalink-set" name="permalink" value="<?php echo $produit->permalink; ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Prix</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="prix" value="<?php echo $produit->prix; ?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Meta Description</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="meta_description" value="<?php echo $produit->meta_description; ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea class="form-control ckeditor" rows="10" name="description"><?php echo $produit->description; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10">
                            <select required class="form-control" name="status">
                                <option value="1" <?php if ($produit->status == 1) {
                                    echo " selected";
                                } ?>>Publier
                                </option>
                                <option value="0" <?php if ($produit->status == 0) {
                                    echo " selected";
                                } ?>>Non Publier
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Categorie</label>
                        <div class="col-sm-10">
                            <select required class="form-control" name="category_id">
                                <?php foreach ($categories as $cat) { ?>
                                    <option <?php if ($produit->category_id === $cat->id) {
                                        echo ' selected ';
                                    } ?>value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Image</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="banner"
                                   accept="image/png, image/jpeg, image/gif"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-grou text-right">
                <button class="btn btn-success btn-icon">
                    <i class="fa-check"></i>
                    <span>Enregistrer</span>
                </button>
            </div>
        </form>
    </div>
</div>