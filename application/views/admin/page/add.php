<div class="page-title">
    <div class="title-env">
        <h1 class="title">Ajouter une page</h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa-home"></i>Tableau de bord</a>
            </li>
            <li class="active ms-hover">
                <strong>Ajouter une page</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <form role="form" enctype="multipart/form-data" method="post" class="form-horizontal" action="<?php echo base_url(); ?>admin/page/add">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Titre</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" placeholder="Titre" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Meta description</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="meta_description" placeholder="Meta description" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Slug(name)</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="slug" placeholder="Slug" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-grou text-right">
                <button class="btn btn-success btn-icon">
                    <i class="fa-check"></i>
                    <span>Ajouter</span>
                </button>
            </div>
        </form>
    </div>
</div>