<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="author" content="Bachouel Maher"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Admin | Brico Pièces</title>
    <meta name="description" content="La partie admin de Brico Pièces"/>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
    <!-- oG iMAGE -->
    <meta property="og:type" content="RB og:type"/>
    <meta property="og:locale" content="ngMetaog:local"/>
    <meta property="og:image" content="<?php echo base_url().'partage.jpg' ?>"/>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
    <link rel="stylesheet" href="<?php asset('admin/css/fonts/linecons/css/linecons.css'); ?>">
    <link rel="stylesheet" href="<?php asset('admin/css/fonts/fontawesome/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?php asset('admin/css/bootstrap.css'); ?>">
    <link rel="stylesheet" href="<?php asset('admin/css/xenon-core.css'); ?>">
    <link rel="stylesheet" href="<?php asset('admin/css/xenon-forms.css'); ?>">
    <link rel="stylesheet" href="<?php asset('admin/css/xenon-components.css'); ?>">
    <link rel="stylesheet" href="<?php asset('admin/css/custom.css'); ?>">
    <link rel="stylesheet" href="<?php asset('admin/css/xenon-skins.css'); ?>">
    <link rel="stylesheet" href="<?php asset('admin/css/jquery-confirm.css'); ?>">
    <link rel="stylesheet" href="<?php asset('admin/js/datatables/dataTables.bootstrap.css'); ?>">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="page-body">
<div class="settings-pane">
    <a href="#" data-toggle="settings-pane" data-animate="true">
        &times;
    </a>
</div>
<div class="page-container">
    <div class="sidebar-menu toggle-others fixed">
        <div class="sidebar-menu-inner">
            <header class="logo-env">
                <!-- logo -->
                <div class="logo">
                    <a href="<?php echo base_url(); ?>admin/dashboard" class="logo-expanded logo-admin">
                        <img class="logo-header" src="<?php echo base_url().'logo-admin.png'; ?>"   alt="Brico Pièces" title="Brico Pièces" />
                    </a>
                    <a href="<?php echo base_url(); ?>admin/dashboard" class="logo-collapsed logo-admin">
                        B-P
                    </a>
                </div>
            </header>
            <ul id="main-menu" class="main-menu">
                <!-- add class "multiple-expanded" to allow multiple submenus to open -->
                <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
                <li class="<?php if ($activepage == "dashboard") {
                    echo "active expanded";
                } ?>">
                    <a href="<?php echo base_url(); ?>admin/dashboard">
                        <i class="linecons-cog"></i>
                        <span class="title">Tableau de bord</span>
                    </a>
                </li>
                <li class="<?php if ($activepage == "blog") {
                    echo "active expanded";
                } ?>">
                    <a href="#">
                        <i class="linecons-doc"></i>
                        <span class="title">Blog</span>
                    </a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/blog/add">
                                <span class="title">Nouveau</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/blog">
                                <span class="title">List</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($activepage == "produits") {
                    echo "active expanded";
                } ?>">
                    <a href="#">
                        <i class="linecons-truck"></i>
                        <span class="title">Produits</span>
                    </a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/produits/add">
                                <span class="title">Nouveau produit</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/produits">
                                <span class="title">List des produits<span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/categories/add">
                                <span class="title">Nouveau categorie</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/categories">
                                <span class="title">List des categories</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($activepage == "pratiques") {
                    echo "active expanded";
                } ?>">
                    <a href="#">
                        <i class="linecons-desktop"></i>
                        <span class="title">Pratiques</span>
                    </a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/pratiques/add">
                                <span class="title">Nouveau</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/pratiques">
                                <span class="title">List</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($activepage == "page") {
                    echo "active expanded";
                } ?>">
                    <a href="#">
                        <i class="linecons-globe"></i>
                        <span class="title">Seo</span>
                    </a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/page/add">
                                <span class="title">Nouveau</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>admin/page">
                                <span class="title">List</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($activepage == "newsletter") {
                    echo "active expanded";
                } ?>">
                    <a href="<?php echo base_url(); ?>admin/newsletter">
                        <i class="linecons-paper-plane"></i>
                        <span class="title">Newsletter</span>
                    </a>
                </li>

            </ul>
        </div>
    </div>
    <div class="main-content">
        <!-- User Info, Notifications and Menu Bar -->
        <nav class="navbar user-info-navbar" role="navigation">
            <ul class="user-info-menu left-links list-inline list-unstyled">
                <li class="hidden-sm hidden-xs">
                    <a href="#" data-toggle="sidebar">
                        <i class="fa-bars"></i>
                    </a>
                </li>
            </ul>
            <!-- Right links for user info navbar -->
            <ul class="user-info-menu right-links list-inline list-unstyled">
                <li class="dropdown user-profile">
                    <a href="#" data-toggle="dropdown">
                        <img src="<?php asset('admin/images/c5.jpg'); ?>" alt="<?php echo $user->firstname .' '. $user->lastname; ?>"
                             class="img-circle img-inline userpic-32"
                             width="30" height="30"/>
                        <span><?php echo $user->lastname.' '. $user->firstname;; ?>
                            <i class="fa-angle-down"></i>
							</span>
                    </a>
                    <ul class="dropdown-menu user-profile-menu list-unstyled">
                        <li>
                            <a href="<?php echo base_url(); ?>admin/profile">
                                <i class="fa-user"></i>
                                Profile
                            </a>
                        </li>
                        <li class="last">
                            <a href="<?php echo base_url() . 'connexion/logout' ?>">
                                <i class="fa-lock"></i>
                                Déconnexion
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
