<!DOCTYPE html>
<html lang="fr">
<?php
error_reporting(0);
$ci =& get_instance();
$page_slug = $ci->uri->segment(1);
$ci->load->model('page_model', 'page');
if($page_slug==="services" && count($ci->uri->segment(2)) ){
	$page_slug=$ci->uri->segment(1).'/'.$ci->uri->segment(2);
	$meta = $ci->page->getMetaByPage($page_slug);
	if (!$meta) {
        $meta = array(
            'meta_title' => 'Brico Pièces ',
            'meta_description' => 'Brico Pièces ',
        );
    } else {
	 $meta = array(
            'meta_title' => $meta[0]->meta_title,
            'meta_description' => $meta[0]->meta_description
        );
	}
}else{
	if (($page_slug || $page_slug == null) && !count($ci->uri->segment(2))) {
    
    if ($page_slug === null) {
        $page_slug = 'accueil';
    }
    $meta = $ci->page->getMetaByPage($page_slug);
    if (!$meta) {
        $meta = array(
            'meta_title' => 'Brico Pièces ',
            'meta_description' => 'Brico Pièces ',
        );
    } else {
        $meta = array(
            'meta_title' => $meta[0]->meta_title,
            'meta_description' => $meta[0]->meta_description
        );
    }
}
}

?>
<html lang="fr-FR">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="index, follow">
    <meta name="theme-color" content="#0063d1" style="color: #0063d1;">
    <meta name="author" content="Maher Bachouel (maherbachouel@gmail.com)">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
	 <meta http-equiv="X-UA-Compatible" content="IE=11" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $meta['meta_title']; ?></title>
    <meta name="description" content="<?php echo $meta['meta_description']; ?>">
   
    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="<?php echo $meta['meta_title']; ?>">
    <meta itemprop="description" content="<?php echo $meta['meta_description']; ?>">
    <meta itemprop="image" content="<?php echo isset($ogImage) ? $ogImage : base_url().'partage.jpg' ?>">

    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="<?php echo base_url(); ?>">
    <meta property="og:type" content="website">
    <meta property="og:title" content="<?php echo $meta['meta_title']; ?>">
    <meta property="og:description" content="<?php echo $meta['meta_description']; ?>">
    <meta property="og:image" content="<?php echo isset($ogImage) ? $ogImage : base_url().'partage.jpg' ?>">

    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="<?php echo $meta['meta_title']; ?>">
    <meta name="twitter:description" content="<?php echo $meta['meta_description']; ?>">
    <meta name="twitter:image" content="<?php echo isset($ogImage) ? $ogImage : base_url().'partage.jpg' ?>">

    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,900" rel="stylesheet">
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?php css_file('bootstrap.min.css') ?>" media="all"/>
    <link rel="stylesheet" type="text/css" href="<?php css_file('font-awesome.min.css') ?>" media="all"/>
    <link rel="stylesheet" type="text/css" href="<?php css_file('bootstrap-grid.min.css') ?>" media="all"/>
    <link rel="stylesheet" type="text/css" href="<?php css_file('bootstrap-reboot.min.css') ?>" media="all"/>
    <link rel="stylesheet" type="text/css" href="<?php css_file('font-brico.css') ?>" media="all"/>
    <link rel="stylesheet" type="text/css" href="<?php css_file('slick.css') ?>" media="all"/>
    <link rel="stylesheet" type="text/css" href="<?php css_file('brico-font-awesome.css') ?>" media="all"/>
    <link rel="stylesheet" type="text/css" href="<?php css_file('slick-style.css') ?>" media="all"/>
    <link rel="stylesheet" type="text/css" href="<?php css_file('animate.min.css') ?>" media="all"/>
    <link rel="stylesheet" type="text/css" href="<?php css_file('style.css') ?>" media="all"/>
    <link rel="stylesheet" type="text/css" href="<?php css_file('colors/blue.css') ?>" media="all"/>
    <!-- Demo Purpose Only. Should be removed in production -->
    <!--link rel="stylesheet" href="<?php css_file('config.css') ?>">
    <link href="<?php css_file('colors/blue.css') ?>" rel="alternate stylesheet" title="Blue color">
    <link href="<?php css_file('colors/flat-green.css') ?>" rel="alternate stylesheet" title="Flat Green color">
    <link href="<?php css_file('colors/green.css') ?>" rel="alternate stylesheet" title="Green color">
    <link href="<?php css_file('colors/orange.css') ?>" rel="alternate stylesheet" title="Orange color">
    <link href="<?php css_file('colors/red.css') ?>" rel="alternate stylesheet" title="Red color">
    <link href="<?php css_file('colors/yellow.css') ?>" rel="alternate stylesheet" title="Yellow color"-->
</head>
<body class="page home page-template-default left-sidebar ">
<div id="page" class="hfeed site">
    <div class="top-bar top-bar-v1">
        <div class="col-full">
            <ul id="menu-top-bar-left" class="nav justify-content-center">
                <li class="menu-item animate-dropdown">
                    <a title="Brico Pièces" href="javascript:void(0)">Brico Pièces</a>
                </li>
                <li class="menu-item animate-dropdown">
                    <a title="Quality Guarantee of products" href="javascript:void(0)">Garantie de qualité des produits</a>
                </li>
                <li class="menu-item animate-dropdown">
                    <a title="Fast returnings program" href="mailto:bricopiecesjerba@gmail.com"><strong>Contact: </strong> bricopiecesjerba@gmail.com</a>
                </li>
                <li class="menu-item animate-dropdown">
                    <a title="No additional fees" href="tel:+21655646217"><strong>Tel: </strong> (+216) 55 64 62 17</a>
                </li>
            </ul>
            <!-- .nav -->
        </div>
        <!-- .col-full -->
    </div>
    <!-- .top-bar-v1 -->
    <header id="masthead" class="site-header header-v1" style="background-image: none; ">
        <div class="col-full desktop-only">
            <div class="techmarket-sticky-wrap">
                <div class="row">
                    <div class="site-branding">
                        <a href="<?php echo base_url(); ?>" class="custom-logo-link" rel="home" title="Brico Pièces">
						<img class="logo-header" src="<?php echo base_url().'logo.png'; ?>"   alt="Brico Pièces" title="Brico Pièces" />
                        </a>
                    </div>
                    <nav id="primary-navigation" class="primary-navigation" aria-label="Primary Navigation" data-nav="flex-menu">
                        <ul id="menu-primary-menu" class="nav yamm">
                            <li class="<?php if ($activePage == 'home') {
                                echo "sale-clr ";
                            } ?> yamm-fw menu-item animate-dropdown">
                                <a title="Accueil" href="<?php echo base_url(); ?>">Accueil</a>
                            </li>
                            <li class="<?php if ($activePage == 'apropos') {
                                echo "sale-clr ";
                            } ?> menu-item animate-dropdown">
                                <a title="À Propos" href="<?php echo base_url() . 'apropos'; ?>">À Propos</a>
                            </li>
                            <li class="<?php if ($activePage == 'produits') {
                                echo "sale-clr ";
                            } ?> yamm-fw menu-item animate-dropdown">
                                <a title="Super deals" href="<?php echo base_url() . 'produits'; ?>">Produits</a>
                            </li>
                            <li class="<?php if (($activePage == 'climatisation') || ($activePage == 'froid') || ($activePage == 'electromenager') || ($activePage == 'chauffage')) {
                                echo "sale-clr ";
                            } ?> menu-item menu-item-has-children animate-dropdown dropdown">
                                <a title="Mother`s Day" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" href="#">Services <span class="caret"></span></a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li class="menu-item animate-dropdown">
                                        <a title="Climatisation" href="<?php echo base_url() . 'services/climatisation'; ?>">Climatisation </a>
                                    </li>
                                    <li class="menu-item animate-dropdown">
                                        <a title="Climatisation" href="<?php echo base_url() . 'services/chauffage'; ?>">Chauffage </a>
                                    </li>
                                    <li class="menu-item animate-dropdown">
                                        <a title="Eléctro-ménager" href="<?php echo base_url() . 'services/electro-menager'; ?>">Électroménager </a>
                                    </li>
                                    <li class="menu-item animate-dropdown">
                                        <a title="Froid" href="<?php echo base_url() . 'services/froid'; ?>">Froid </a>
                                    </li>
                                </ul>
                                <!-- .dropdown-menu -->
                            </li>
                            <li class="menu-item animate-dropdown <?php if ($activePage == 'pratique') {
                                echo "sale-clr ";
                            } ?>">
                                <a title="Headphones Sale" href="<?php echo base_url() . 'pratiques'; ?>">Pratiques</a>
                            </li>
                            <li class="menu-item animate-dropdown <?php if ($activePage == 'blog') {
                                echo "sale-clr ";
                            } ?>">
                                <a title="Headphones Sale" href="<?php echo base_url() . 'blog'; ?>">Blog</a>
                            </li>
                            <li class="menu-item animate-dropdown <?php if ($activePage == 'contact') {
                                echo "sale-clr ";
                            } ?>">
                                <a title="Headphones Sale" href="<?php echo base_url() . 'contact'; ?>">Contact</a>
                            </li>
                            <li class="techmarket-flex-more-menu-item dropdown">
                                <a title="..." href="#" data-toggle="dropdown" class="dropdown-toggle">...</a>
                                <ul class="overflow-items dropdown-menu"></ul>
                                <!-- . -->
                            </li>
                        </ul>
                        <!-- .nav -->
                    </nav>
                    <!-- .primary-navigation -->
                    <nav id="secondary-navigation" class="secondary-navigation" aria-label="Secondary Navigation" data-nav="flex-menu">
                        <ul id="menu-secondary-menu" class="nav">
                            <li class="menu-item">
                                <?php
                                $ci =& get_instance();
                                $loggedIn = (bool)$ci->session->userdata('loggedIn');
                                ?>
                                <?php if ($loggedIn) { ?>
                                    <a title="Administrateur" href="<?php echo base_url() . 'connexion'; ?>">
                                        <i class="tm tm-login-register"></i><span style="vertical-align: middle;
    margin-top: 5px;
    display: inline-block;">Admin</span></a>
                                <?php } else { ?>
                                    <a title="Connexion" href="<?php echo base_url() . 'connexion'; ?>">
                                        <i class="tm tm-login-register"></i><span>Connexion</span></a>
                                <?php } ?>
                            </li>
                        </ul>
                        <!-- .nav -->
                    </nav>
                    <!-- .secondary-navigation -->
                </div>
                <!-- /.row -->
            </div>
            <div class="row align-items-center">
                <div id="departments-menu" class="dropdown departments-menu" <?php echo $filter; ?> >
                    <button class="btn dropdown-toggle btn-block" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="tm tm-departments-thin"></i>
                        <span>Toutes les catégories</span>
                    </button>
                    <ul id="menu-departments-menu" class="dropdown-menu yamm departments-menu-dropdown">
                        <?php foreach ($categories as $cat) { ?>
                            <li class=" menu-item animate-dropdown">
                                <a title="<?php echo $cat->name; ?>" href="<?php echo base_url() . 'produits/' . $cat->permalink; ?>"><?php echo $cat->name; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <!-- .departments-menu -->
                <form class="navbar-search" method="get" action="<?php echo base_url(); ?>search">
                    <label class="sr-only screen-reader-text" for="search">Recherche:</label>
                    <div class="input-group">
                        <input type="text" autocomplete="off" required id="search" class="form-control search-field product-search-field" dir="ltr" name="query" placeholder="Rechercher des produits/catégories..."/>
                        <!-- .input-group-addon -->
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-search"></i>
                                <span class="search-btn">Rechercher</span>
                            </button>
                        </div>
                        <!-- .input-group-btn -->
                    </div>
                    <!-- .input-group -->
                </form>

            </div>
            <!-- /.row -->
        </div>
        <!-- .col-full -->
        <div class="col-full handheld-only">
            <div class="handheld-header">
                <div class="row">
                    <div class="site-branding">
                        <a href="<?php echo base_url(); ?>" class="custom-logo-link" rel="home" title="Brico Pièces">
                            <img class="logo-header" src="<?php echo base_url().'logo.png'; ?>"   alt="Brico Pièces" title="Brico Pièces" />
                        </a>
                        <!-- /.custom-logo-link -->
                    </div>
                    <!-- /.site-branding -->
                    <!-- ============================================================= End Header Logo ============================================================= -->
                    <div class="handheld-header-links">
                        <ul class="columns-3">
                            <li class="my-account">
                                <a href="<?php echo base_url() . 'connexion' ?>" class="has-icon">
                                    <i class="tm tm-login-register"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- .columns-3 -->
                    </div>
                    <!-- .handheld-header-links -->
                </div>
                <!-- /.row -->
                <div class="techmarket-sticky-wrap">
                    <div class="row">
                        <nav id="handheld-navigation" class="handheld-navigation" aria-label="Handheld Navigation">
                            <button class="btn navbar-toggler" type="button">
                                <i class="tm tm-departments-thin"></i>
                                <span>Menu</span>
                            </button>
                            <div class="handheld-navigation-menu">
                                <span class="tmhm-close">Close</span>
                                <ul id="menu-primary-menu" class="nav yamm">
                                    <li class="<?php if ($activePage == 'home') {
                                        echo "sale-clr ";
                                    } ?> yamm-fw menu-item animate-dropdown">
                                        <a title="Accueil" href="<?php echo base_url(); ?>">Accueil</a>
                                    </li>
                                    <li class="<?php if ($activePage == 'apropos') {
                                        echo "sale-clr ";
                                    } ?> menu-item animate-dropdown">
                                        <a title="À Propos" href="<?php echo base_url() . 'apropos'; ?>">À Propos</a>
                                    </li>
                                    <li class="<?php if ($activePage == 'produits') {
                                        echo "sale-clr ";
                                    } ?> yamm-fw menu-item animate-dropdown">
                                        <a title="Super deals" href="<?php echo base_url() . 'produits'; ?>">Produits</a>
                                    </li>
                                    <li class="<?php if (($activePage == 'climatisation') || ($activePage == 'froid') || ($activePage == 'electromenager') || ($activePage == 'chauffage')) {
                                        echo "sale-clr ";
                                    } ?> menu-item menu-item-has-children animate-dropdown dropdown">
                                        <a title="Mother`s Day" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" href="#">Services <span class="caret"></span></a>
                                        <ul role="menu" class=" dropdown-menu">
                                            <li class="menu-item animate-dropdown">
                                                <a title="Climatisation" href="<?php echo base_url() . 'services/climatisation'; ?>">Climatisation </a>
                                            </li>
                                            <li class="menu-item animate-dropdown">
                                                <a title="Climatisation" href="<?php echo base_url() . 'services/chauffage'; ?>">Chauffage </a>
                                            </li>
                                            <li class="menu-item animate-dropdown">
                                                <a title="Eléctro-ménager" href="<?php echo base_url() . 'services/electro-menager'; ?>">Électroménager </a>
                                            </li>
                                            <li class="menu-item animate-dropdown">
                                                <a title="Froid" href="<?php echo base_url() . 'services/froid'; ?>">Froid </a>
                                            </li>
                                        </ul>
                                        <!-- .dropdown-menu -->
                                    </li>
                                    <li class="menu-item animate-dropdown <?php if ($activePage == 'pratique') {
                                        echo "sale-clr ";
                                    } ?>">
                                        <a title="Headphones Sale" href="<?php echo base_url() . 'pratiques'; ?>">Pratiques</a>
                                    </li>
                                    <li class="menu-item animate-dropdown <?php if ($activePage == 'blog') {
                                        echo "sale-clr ";
                                    } ?>">
                                        <a title="Headphones Sale" href="<?php echo base_url() . 'blog'; ?>">Blog</a>
                                    </li>
                                    <li class="menu-item animate-dropdown <?php if ($activePage == 'contact') {
                                        echo "sale-clr ";
                                    } ?>">
                                        <a title="Headphones Sale" href="<?php echo base_url() . 'contact'; ?>">Contact</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- .handheld-navigation-menu -->
                        </nav>
                        <!-- .handheld-navigation -->
                        <div class="site-search">
                            <div class="widget woocommerce widget_product_search">
                                <form role="search" method="get" class="woocommerce-product-search" action="<?php echo base_url(); ?>search">
                                    <label class="screen-reader-text" for="woocommerce-product-search-field-0">Rechercher:</label>
                                    <input required autocomplete="off" type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Rechercher des produits/catégories..." name="query"/>
                                    <input type="submit" value="Rechercher"/>
                                </form>
                            </div>
                            <!-- .widget -->
                        </div>
                        <!-- .site-search -->

                    </div>
                    <!-- /.row -->
                </div>
            </div>
            <!-- .handheld-header -->
        </div>
        <!-- .handheld-only -->
    </header>
