<div class="page-title">
    <div class="title-env">
        <h1 class="title">Liste des categories</h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa-home"></i>Tableau de bord</a>
            </li>
            <li class="active ms-hover">
                <strong>Liste des categories</strong>
            </li>
        </ol>
    </div>
</div>
<?php if (count($categories)) { ?>
    <div class="row">
        <div class="col-sm-12">
            <!--Basic Setup -->
            <div class="panel panel-default">
                <div class="panel-body">
                    <table id="admindatatable" class="dataTable table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nom</th>
                            <th>Permalink</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($categories as $cat) {
                            ?>
                            <tr>
                                <td><?php echo $cat->id; ?></td>
                                <td><?php echo $cat->name; ?></td>
                                <td><?php echo $cat->permalink; ?></td>
                                <td>
                                    <a href="<?php echo base_url(); ?>admin/categories/edit/<?php echo $cat->id; ?>"
                                       data-id="<?php echo $cat->id; ?>"><i
                                            class="icon-admin linecons-pencil"></i></a>
                                    <a href="#" class="confirm-modal" data-href="<?php echo base_url(); ?>admin/categories/delete"
                                       data-id="<?php echo $cat->id; ?>"><i
                                            class="icon-admin fa-times-circle-o"></i></a></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php } else {
    ?>
    <div class="no-data">
        <div class="no-data-icon">
            <i class="fa fa-exclamation"></i>
        </div>
        <p class="no-data-text">No items in the list</p>
        <a href="<?php echo base_url(); ?>admin/categories/add" class="no-data-link">Ajouter une categorie</a>
    </div>
<?php } ?>