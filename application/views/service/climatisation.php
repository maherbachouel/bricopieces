<div id="content" class="site-content">
    <div class="col-full">
        <div class="row">
            <nav class="woocommerce-breadcrumb">
                <a href="<?php echo base_url(); ?>">Accueil</a>	<span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
			<a href="<?php echo base_url().'services'; ?>">Services</a>
                <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                Climatisation
            </nav>
            <!-- .woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="type-page hentry">
                        <header class="entry-header">
                            <div class="page-header-caption">
                                <h1 class="entry-title">Services de climatisation</h1>
                            </div>
                        </header>
                        <div class="entry-content">
                            <section class="section terms-conditions">
                                <div class="section-media-single-banner" id="section-media-single-banner3">
                                    <div class="media">
                                        <img alt="Services de climatisation" src="<?php img_file('products/climatistation.jpg'); ?>" class="d-flex align-self-center">
                                        <div class="media-body">
                                            <!--h2 class="section-title">QLED - The Next Generation
                                                <br>in Televisions</h2-->
                                            <div class="description">
                                                <p>Votre système de climatisation domicile ou au bureau ne fonctionne-t-il pas correctement ou a-t-il complètement cessé de fonctionner en raison d'une fuite de réfrigérant, d'un problème de capteur, d'une défaillance du contrôle électrique, de problèmes de drainage, d'une défaillance de l'unité extérieure, etc.? </p>
                                                <p>Nos techniciens certifiés, compétents et expérimentés peuvent évaluer votre Les problèmes de climatisation à fond et vous fournir le correctif nécessaire. Nous fournissons également un service de réparation 6j / 7.</p>
                                                <p>Nous offrons une large gamme de services de climatisation professionnels </p>
                                                <ul>
												<li>Ventes unitaires</li>
												<li>Installations</li>
												<li>Principales marques de qualité</li>
												<li>Entretien</li>
												<li>Réparations</li>
												<li>Mise hors service et enlèvement d'unités</li>
												<li>Déplacement d'unités</li>
												<li>Contrats de service</li>
                                                </ul>
                                            </div>
                                            <!-- .description -->
                                        </div>
                                        <!-- .media-body -->
                                    </div>
                                    <!-- .media -->
                                </div>
                            </section>
                            <!-- .terms-conditions -->
                        </div>
                        <!-- .entry-content -->
                    </div>
                    <!-- .hentry -->
                </main>
            </div>
        </div>
    </div>
</div>