<footer class="main-footer sticky footer-type-1">
    <div class="footer-inner">
        <!-- Add your copyright text here -->
        <div class="footer-text">
            <strong>© <a href="<?php echo base_url().'admin/dashboard'; ?>">Brico-pieces.com</a> <script>document.write(new Date().getFullYear());</script>. Tous droits réservés.</strong>
        </div>
        <!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
        <div class="go-up">
            <a href="#" rel="go-top">
                <i class="fa-angle-up"></i>
            </a>
        </div>
    </div>
</footer>
</div>
</div>
<div class="page-loading-overlay">
    <div class="loader-2"></div>
</div>
<script src="<?php asset('admin/js/jquery-1.11.1.min.js'); ?>"></script>
<script src="<?php asset('admin/js/bootstrap.min.js'); ?>"></script>
<script src="<?php asset('admin/js/TweenMax.min.js'); ?>"></script>
<script src="<?php asset('admin/js/resizeable.js'); ?>"></script>
<script src="<?php asset('admin/js/joinable.js'); ?>"></script>
<script src="<?php asset('admin/js/xenon-api.js'); ?>"></script>
<script src="<?php asset('admin/js/xenon-toggles.js'); ?>"></script>
<script src="<?php asset('admin/js/xenon-widgets.js'); ?>"></script>
<script src="<?php asset('admin/js/devexpress-web-14.1/js/globalize.min.js'); ?>"></script>
<script src="<?php asset('admin/js/devexpress-web-14.1/js/dx.chartjs.js'); ?>"></script>
<script src="<?php asset('admin/js/toastr/toastr.min.js'); ?>"></script>
<!-- JavaScripts datatable -->
<script src="<?php asset('admin/js/datatables/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php asset('admin/js/datatables/dataTables.bootstrap.js'); ?>"></script>
<script src="<?php asset('admin/js/datatables/yadcf/jquery.dataTables.yadcf.js'); ?>"></script>
<script src="<?php asset('admin/js/datatables/tabletools/dataTables.tableTools.min.js'); ?>"></script>
<script src="<?php asset('admin/js/jquery-confirm/js/jquery-confirm.js'); ?>"></script>
<script src="<?php asset('admin/js/custom.js'); ?>"></script>

<script src="<?php asset('admin/js/ckeditor/ckeditor.js'); ?>"></script>
<script src="<?php asset('admin/js/ckeditor/adapters/jquery.js'); ?>"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php asset('admin/js/xenon-custom.js'); ?>"></script>
<script src="<?php asset('admin/js/jquery.slugify.js'); ?>"></script>
</body>
</html>