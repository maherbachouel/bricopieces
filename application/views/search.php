<div id="content" class="site-content">
    <div class="col-full">
        <div class="row">
            <nav class="woocommerce-breadcrumb">
                <a href="<?php echo base_url(); ?>">Accueil</a>
                <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                Résultat De Recherche
            </nav>
            <!-- .woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="type-page hentry">
                        <header class="entry-header">
                            <div class="page-header-caption">
                                <h1 class="entry-title">Résultat De Recherche</h1>
                            </div>
                        </header>
                        <div class="about-accordion col-md-12 col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div id="accordion" role="tablist" aria-multiselectable="true">
                                        <?php
                                        if (count($resultas)) {
                                            foreach ($resultas as $key => $result) { ?>
                                                <div class="card">
                                                    <div class="card-header" role="tab" id="headingOne">
                                                        <h5 class="mb-0">
                                                            <a
                                                                class="collapsed" data-toggle="collapse"
                                                                data-parent="#accordion" href="#BPR<?php echo $key; ?>"
                                                                aria-expanded="true" aria-controls="collapseTwo">
                                                                <i class="fa-icon"></i>
                                                                <?php echo $result->name; ?>
                                                            </a>
                                                        </h5>
                                                    </div>
                                                    <!-- .card-header -->
                                                    <div id="BPR<?php echo $key; ?>" class="collapse"
                                                         role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="card-block">
                                                            <div>
                                                                <?php echo isset($result->description) ? (strlen($result->description) > 200 ? substr($result->description, 0, 200) . '...' : $result->description) : $result->name ?>

                                                            </div>
                                                            <div class="read-more-search">
                                                                <a class="button"
                                                                   href="<?php echo $result->permalink; ?>">Voir Détails</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- .collapse -->
                                                </div>
                                            <?php }
                                        } else { ?>
                                            <div class="liste-not-found">
                                                <p>Désolé, rien ne correspond à vos termes de recherche</p>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>