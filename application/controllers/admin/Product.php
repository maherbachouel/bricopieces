<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_Controller.php');

class Product extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('product_model', 'product');
        $this->load->model('category_model', 'category');
    }


    public function index()
    {
        $data = array(
            "user" => $this->user,
            "activepage" => "produits",
            'produits' => $this->product->getProducts(),
            'published' =>  $this->product->count(array("status" => 1)),
            'draft' =>  $this->product->count(array("status" => 0))
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/produits/liste', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            $data = array(
                "name" => $this->input->post('name'),
                "description" => $this->input->post('description'),
                "status" => $this->input->post('status'),
                "meta_description" => $this->input->post('meta_description'),
                "prix" => $this->input->post('prix'),
                "category_id" => $this->input->post('category_id'),
                "permalink" => $this->input->post('permalink'),
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );
            if (isset($_FILES['banner']['name']) && $_FILES['banner']['name']) {
                $upload_dir = FCPATH . 'uploads/';
                $formations_dir = $upload_dir . 'product/';
                $image_dir = 'uploads/product/';
                if (!is_dir($upload_dir)) {
                    mkdir($upload_dir);
                }
                if (!is_dir($formations_dir)) {
                    mkdir($formations_dir);
                }
                $filename = time();
                $extension = strtolower(pathinfo($_FILES['banner']['name'], PATHINFO_EXTENSION));
                $image = $filename . '.' . $extension;

                if (!move_uploaded_file($_FILES['banner']['tmp_name'], $formations_dir . $image)) {
                    $errors[] = 'ATTACHMENT_FILE_NOT_VALID';
                    $image = null;
                } else {
                    $data['banner'] = $image_dir . $image;
                }
            }
            $this->product->AddProduct($data);
            redirect(base_url() . 'admin/produits');
        }
        $data = array(
            "user" => $this->user,
            "activepage" => "produits",
            'categories' => $this->category->read('*')
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/produits/add', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }

    public function edit($id)
    {

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $data = array(
                "name" => $this->input->post('name'),
                "description" => $this->input->post('description'),
                "status" => $this->input->post('status'),
                "meta_description" => $this->input->post('meta_description'),
                "prix" => $this->input->post('prix'),
                "category_id" => $this->input->post('category_id'),
                "permalink" => $this->input->post('permalink'),
                "updated_at" => date('Y-m-d H:i:s')
            );
            if (isset($_FILES['banner']['name']) && $_FILES['banner']['name']) {
                $upload_dir = FCPATH . 'uploads/';
                $formations_dir = $upload_dir . 'product/';
                $image_dir = 'uploads/product/';
                if (!is_dir($upload_dir)) {
                    mkdir($upload_dir);
                }
                if (!is_dir($formations_dir)) {
                    mkdir($formations_dir);
                }
                $filename = time();
                $extension = strtolower(pathinfo($_FILES['banner']['name'], PATHINFO_EXTENSION));
                $image = $filename . '.' . $extension;

                if (!move_uploaded_file($_FILES['banner']['tmp_name'], $formations_dir . $image)) {
                    $errors[] = 'ATTACHMENT_FILE_NOT_VALID';
                    $image = null;
                } else {
                    $data['banner'] = $image_dir . $image;
                }
            }
            $this->product->EditProduct($data, $id);
            redirect(base_url() . 'admin/produits');
        }

        if (!$this->product->getProductById($id)) {
            redirect(base_url() . 'admin/produits');
        }
        $data = array(
            "user" => $this->user,
            "produit" => $this->product->getProductById($id)[0],
            'categories' => $this->category->read('*'),
            "activepage" => 'produits'
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/produits/edit', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }

    public function delete($id)
    {
        $this->product->delete(array('id' => $id));
        redirect(base_url() . 'admin/produits');
    }
}
