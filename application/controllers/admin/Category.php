<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_Controller.php');

class Category extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('category_model', 'category');
        $this->load->model('product_model', 'product');
    }


    public function index()
    {
        $data = array(
            "user" => $this->user,
            "activepage" => "produits",
            'categories' => $this->category->read('*'),
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/categories/liste', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            $data = array(
                "name" => $this->input->post('name'),
                "meta_description" => $this->input->post('meta_description'),
                "permalink" => $this->input->post('permalink'),
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );
            $this->category->AddCategory($data);
            redirect(base_url() . 'admin/categories');
        }
        $data = array(
            "user" => $this->user,
            "activepage" => "produits",
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/categories/add', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }

    public function edit($id)
    {

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $data = array(
                "name" => $this->input->post('name'),
                "meta_description" => $this->input->post('meta_description'),
                "permalink" => $this->input->post('permalink'),
                "updated_at" => date('Y-m-d H:i:s')
            );
            $this->category->EditCategory($data, $id);
            redirect(base_url() . 'admin/categories');
        }
        if (!$this->category->getCategoryById($id)) {
            redirect(base_url() . 'admin/categories');
        }

        $data = array(
            "user" => $this->user,
            "categorie" => $this->category->getCategoryById($id)[0],
            "activepage" => 'produits'
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/categories/edit', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }

    public function delete($id)
    {
        $this->product->delete(array('category_id' => $id));
        $this->category->delete(array('id' => $id));
        redirect(base_url() . 'admin/categories');
    }
}
