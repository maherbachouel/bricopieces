<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->library('encryption');
        $this->load->model('category_model', 'category');
    }

    public function index()
    {
        $notification = '';
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $notification = false;
            $data = array(
                'name' => $this->input->post('name', TRUE),
                'email' => $this->input->post('email', TRUE),
                'phone' => $this->input->post('phone', TRUE),
                'subject' => $this->input->post('subject', TRUE),
                'message' => $this->input->post('message', TRUE),
            );
            $config = $this->config->item('email');
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from($config['smtp_user'], 'Brico Pièces');
            $this->email->to($config['smtp_user']);
            $this->email->subject($data['subject']);
            $message = 'Email: ' . $data['email'] . "<br><br>" . 'Nom: ' . $data['name'] . "<br><br>" . 'Téléphone: ' . $data['phone'] . "<br><br>" . 'Sujet: ' . $data['subject'] . "<br><br>" . 'Message: ' . $data['message'];
            $this->email->message($message);

            if ($this->email->send()) {
                $notification = true;
            }
            //$notification = true;
        }

        $data = array(
            'activePage' => 'contact',
            'filter' => '',
            'categories' => $this->category->read('*'),
            'notification' => $notification,
        );
        $view = $this->load->view('common/header', $data, TRUE);
        $view .= $this->load->view('contact', $data, TRUE);
        $view .= $this->load->view('common/footer', $data, TRUE);
        echo $view;
    }


}
