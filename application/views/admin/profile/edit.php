<div class="page-title">
    <div class="title-env">
        <h1 class="title">Mon profile</h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa-home"></i>Tableau de bord</a>
            </li>
            <li class="active ms-hover">
                <strong>Mon profile</strong>
            </li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="row">
        <br/>
        <div class="col-md-12">
            <form id="basicForm" enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>admin/profile/edit/<?php echo $user->api_key; ?>" class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Username <span class="asterisk">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="username" class="form-control" value="<?php if (is_object($user)) { echo $user->username; } ?>" placeholder="Username..." required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nom  <span class="asterisk">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="firstname" class="form-control" value="<?php if (is_object($user)) { echo $user->firstname; } ?>" placeholder="Nom..." required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Prénom <span class="asterisk">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="lastname" class="form-control" value="<?php if (is_object($user)) { echo $user->lastname; } ?>" placeholder="Prénom..." required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" name="email" class="form-control" value="<?php if (is_object($user)) { echo $user->email; } ?>" placeholder="Email..."/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mot de passe</label>
                            <div class="col-sm-9">
                                <input type="text" name="password" class="form-control" value="<?php if (is_object($user)) {
                                    $ci =& get_instance();
                                    echo $this->encryption->decrypt($user->password);
                                } ?>" placeholder="Mot de passe..."/>
                            </div>
                        </div>
                    </div>
                    <div class="form-grou text-right">
                        <button class="btn btn-success btn-icon">
                            <i class="fa-check"></i>
                            <span>Enregistrer</span>
                        </button>
                    </div>
                </div><!-- panel -->
            </form>
        </div><!-- col-md-6 -->
    </div>
</div>
