<div id="content" class="site-content">
    <div class="col-full">
        <div class="row">
            <nav class="woocommerce-breadcrumb">
                <a href="<?php echo base_url(); ?>">Accueil</a>	<span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
			<a href="<?php echo base_url().'services'; ?>">Services</a>
                <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                Électroménager
            </nav>
            <!-- .woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="type-page hentry">
                        <header class="entry-header">
                            <div class="page-header-caption">
                                <h1 class="entry-title">Electroménager </h1>
                            </div>
                        </header>
                        <div class="entry-content">
                            <section class="section terms-conditions">
                                <div class="section-media-single-banner" id="section-media-single-banner3">
                                    <div class="media">
                                        <img alt="Services d'électroménager" src="<?php img_file('products/electromenager.jpg'); ?>" class="d-flex align-self-center">
                                        <div class="media-body">
                                            <!--h2 class="section-title">QLED - The Next Generation
                                                <br>in Televisions</h2-->
                                            <div class="description">
                                                <p>Experts en réparation d’appareils ménagers, nous sommes fiers de notre historique d’excellence du service et considérons nos clients comme notre atout le plus précieux. </p>
                                                <p>Nous avons élargi notre domaine d'expertise pour offrir à nos clients une solution clé en main complète pour tous leurs besoins domestiques, commerciaux d'appareils ménagers et de climatisation.</p>
                                                <p>Nous réparons et entretenons les appareils électroménagers suivants</p>
                                                <ul>
												<li>Machines à laver</li>
												<li>Sèche-linge</li>
												<li>Les réfrigérateurs</li>
												<li>Congélateurs</li>
												<!--li>Machines à glaçons</li-->
												<!--li>Machines à café</li-->
												<li>Lave-vaisselle</li>
												<li>Fours</li>
												<li>Poêles</li>
												<li>Micro-ondes</li>
												<li>Extracteurs</li>
												<li>Aspirateurs</li>
												<li>Autres équipements...</li>
                                                </ul>
                                            </div>
                                            <!-- .description -->
                                        </div>
                                        <!-- .media-body -->
                                    </div>
                                    <!-- .media -->
                                </div>
                            </section>
                            <!-- .terms-conditions -->
                        </div>
                        <!-- .entry-content -->
                    </div>
                    <!-- .hentry -->
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
        <!-- .row -->
    </div>
    <!-- .col-full -->
</div>