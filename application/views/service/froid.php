<div id="content" class="site-content">
<div class="col-full">
    <div class="row">
        <nav class="woocommerce-breadcrumb">
            <a href="<?php echo base_url(); ?>">Accueil</a>
			<span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
			<a href="<?php echo base_url().'services'; ?>">Services</a>
            <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
           Froid
        </nav>
        <!-- .woocommerce-breadcrumb -->
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="type-page hentry">
                    <header class="entry-header">
                        <div class="page-header-caption">
                            <h1 class="entry-title">Service froid </h1>
                             <p class="entry-subtitle"></p>
                        </div>
                    </header>
                    <div class="entry-content">
                        <section class="section terms-conditions">
                            <div class="section-media-single-banner" id="section-media-single-banner3">
                                <div class="media">
                                    <img alt="Service froid" src="<?php img_file('products/froid.jpg'); ?>" class="d-flex align-self-center">
                                    <div class="media-body">
                                        <!--h2 class="section-title">QLED - The Next Generation
                                            <br>in Televisions</h2-->
                                        <div class="description">
                                            <p>Lorsque vous nous confiez votre appareil de réfrigération,  nos techniciens experts apportent le plus grand soin pour vous livrer une machine en parfait état de fonctionnement. D'une réparation intensive d’une fontaine d’eau fraiche à un réfrigérateur nécessitant un réglage, nos techniciens vous traiteront, vous et votre appareil, avec respect. </p>
                                            <p>En fait, en raison de notre niveau de service client supérieur, beaucoup de nos clients se tournent vers Brico Pièces en tant que fournisseur de services de réparation d’appareils de réfrigération de choix.</p>
                                            <p>Nous sommes impatients de vous accueillir en tant que client et de devenir votre source privilégiée pour le service de réparation d'appareils Froid.</p>
                                            <p>Votre appareil est minutieusement testé sur nos bancs d’essai afin de nous assurer qu’il n’y a ni défaut ni omission. </p>
                                            <p>Nous assurons les réparations, services et installations sur les éléments suivants:</p>
                                            <ul>
												 <li>Chambres froides</li>
												 <li>Congélateurs </li>
												 <li>Installations de refroidissement </li>
												 <li>Tous les réfrigérateurs / congélateurs commerciaux </li>
												 <li>Cave à vin </li>
												 <li>Réfrigérateurs De Bar </li>

                                              
                                                
                                            </ul>
                                        </div>
                                        <!-- .description -->
                                    </div>
                                    <!-- .media-body -->
                                </div>
                                <!-- .media -->
                            </div>
                        </section>
                        <!-- .terms-conditions -->
                    </div>
                    <!-- .entry-content -->
                </div>
                <!-- .hentry -->
            </main>
            <!-- #main -->
        </div>
        <!-- #primary -->
    </div>
    <!-- .row -->
</div>
<!-- .col-full -->
</div>