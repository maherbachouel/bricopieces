<?php
function js_file($url) {
    echo base_url() . 'assets/js/' . $url;
}

function img_file($url) {
    echo base_url() . 'assets/images/' . $url;
}

function css_file($url) {
    echo base_url() . 'assets/css/' . $url;
}

function vendors_file($url) {
    echo base_url() . 'assets/vendors/' . $url;
}

function font_file($url) {
    echo base_url() . 'assets/fonts/' . $url;
}

function asset($url) {
    echo base_url() . 'assets/' . $url;
}
