<div id="content" class="site-content">
<div class="col-full">
    <div class="row">
        <nav class="woocommerce-breadcrumb">
            <a href="<?php echo base_url(); ?>">Accueil</a>	<span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
			<a href="<?php echo base_url().'services'; ?>">Services</a>
            <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
           Chauffage
        </nav>
        <!-- .woocommerce-breadcrumb -->
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="type-page hentry">
                    <header class="entry-header">
                        <div class="page-header-caption">
                            <h1 class="entry-title">Chauffage</h1>
                        </div>
                    </header>
                    <div class="entry-content">
                        <section class="section terms-conditions">
                            <div class="section-media-single-banner" id="section-media-single-banner3">
                                <div class="media">
                                    <img alt="services chauffage" src="<?php img_file('products/chauffage.jpg'); ?>" class="d-flex align-self-center">
                                    <div class="media-body">
                                        <!--h2 class="section-title">QLED - The Next Generation
                                            <br>in Televisions</h2-->
                                        <div class="description">
                                            <p>Bien que les étés de la Tunisie soient connus pour être chauds et humides, les hivers peuvent être un peu froids! Un système de chauffage fiable est essentiel pour passer confortablement les mois les plus froids. </p>
                                            <p>Notre service à la clientèle hors pair et notre expertise professionnelle font de nous un choix judicieux pour tous vos besoins de chauffage.</p>
                                             <h2 class="section-title">Nos services de chauffage</h2>
											<p>Bien que votre système de chauffage ne fonctionne peut-être pas aussi souvent que votre climatiseur, les résidents et les propriétaires d’entreprise en Tunisie ne sont pas étrangers au froid du front pendant les mois d’hiver. </p>
                                            <p>Lorsque vous nous contactez pour un service de chauffage, vous pouvez vous attendre à ce que nous vous offrions:</p>
                                            <ul>
											 <li>Réparation de chauffage sur toutes les marques et modèles de systèmes</li>
											 <li>Installation de chauffage sur toutes les marques et modèles de systèmes</li>
											 <li>Entretien préventif de votre système de chauffage</li>
											 <li>Service de chauffage d'urgence 24/7/365</li>
                                            </ul>
                                        </div>
                                        <!-- .description -->
                                    </div>
                                    <!-- .media-body -->
                                </div>
                                <!-- .media -->
                            </div>
                        </section>
                        <!-- .terms-conditions -->
                    </div>
                    <!-- .entry-content -->
                </div>
                <!-- .hentry -->
            </main>
            <!-- #main -->
        </div>
        <!-- #primary -->
    </div>
    <!-- .row -->
</div>
<!-- .col-full -->
</div>