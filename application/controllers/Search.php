<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('search_model', 'search');
        $this->load->model('category_model', 'category');
    }

    public function index()
    {
        $keyword = $this->input->get('query');
        if ($keyword) {
            $resultas = $this->search->getResultSearch($keyword);
        } else {
            $resultas = array();
        }
        $data = array(
            'activePage' => '',
            'resultas' => $resultas,
            'filter'=>'',
            'categories' => $this->category->read('*'),
        );
        $view = $this->load->view('common/header', $data, TRUE);
        $view .= $this->load->view('search', $data, TRUE);
        $view .= $this->load->view('common/footer', $data, TRUE);
        echo $view;
    }
}

