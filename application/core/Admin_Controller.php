<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->library('encryption');
        $loggedIn = (bool)$this->session->userdata('loggedIn');
        if (!$loggedIn) {
            redirect(base_url() . 'connexion');
        }
        $id = $this->session->userdata('uid');
        $api_key = $this->session->userdata('ukey');
        if ($id) {
            $this->load->model('user_model', 'user');
            $this->user = $this->user->GetUserByIdKey($id,$api_key)[0];
        }else{
            redirect(base_url() . 'connexion');
        }
    }
}