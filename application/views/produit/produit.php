<div id="content" class="site-content" tabindex="-1">
    <div class="col-full">
        <div class="row">
            <nav class="woocommerce-breadcrumb">
                <a href="<?php echo base_url(); ?>">Accueil</a>
                <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>Produits
            </nav>
            <!-- .woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <!-- .shop-control-bar -->
                    <div class="tab-content row">
                        <div id="grid" class="tab-pane active col-md-3" role="tabpanel">
                            <div id="" class="widget-area shop-sidebar" role="complementary">
                                <div class="widget woocommerce widget_product_categories techmarket_widget_product_categories" id="techmarket_product_categories_widget-2">
                                    <ul class="product-categories custom-left-bar-cat ">
                                        <li class="product_cat">
                                            <span>Parcourir les catégories</span>
                                            <ul>
                                                <?php foreach ($categories as $cat) {
                                                    $class = '';
                                                    if ($permalink === $cat->permalink) {
                                                        $class = ' active';
                                                    }
                                                    ?>
                                                    <li class="cat-item <?php echo $class; ?>">
                                                        <a href="<?php echo base_url() . 'produits/' . $cat->permalink; ?>" title="<?php echo $cat->name; ?>">
                                                            <?php if ($permalink === $cat->permalink) { ?>
                                                                <span class="child-indicator"><i class="fa fa-angle-right"></i></span>
                                                            <?php } ?>
                                                            <?php echo $cat->name; ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="woocommerce columns-5  col-md-9">
                            <div class="products all-products">
                                <!-- .product -->
                                <?php
                                if (count($produits)) {
                                    foreach ($produits as $product) { ?>
                                        <div class="product product--produits-page ">
                                            <div class="yith-wcwl-add-to-wishlist">
                                                <a href="javascript:void(0)" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                            </div>
                                            <!-- .yith-wcwl-add-to-wishlist -->
                                            <a class="woocommerce-LoopProduct-link woocommerce-loop-product__link" href="<?php echo base_url() . 'produits/' . $product->category_permalink . '/' . $product->product_permalink; ?>">
                                                <img width="224" height="197" title="<?php echo $product->product_name; ?>"
                                                     alt="<?php echo $product->product_name; ?>"
                                                     class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                                     src="<?php echo base_url() . $product->product_banner; ?>">
                                                <!--span class="price">
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span class="woocommerce-Price-currencySymbol">Prix: <?php echo $product->product_prix; ?></span> TND</span>
                                                        </span-->
                                                <h2 class="woocommerce-loop-product__title"><?php echo $product->product_name; ?></h2>
                                            </a>
                                            <!-- .woocommerce-LoopProduct-link -->
                                            <div class="hover-area">
                                                <a class="button" href="<?php echo base_url() . 'produits/' . $product->category_permalink . '/' . $product->product_permalink; ?>">Voir Détails</a>
                                            </div>
                                            <!-- .hover-area -->
                                        </div>
                                    <?php }
                                } else {?>
                                    <div class="liste-not-found">
                                        <p>Aucun produit dans cette catégorie</p>
                                    </div>
                                <?php } ?>
                                <!-- .product -->
                            </div>
                            <!-- .products -->
                        </div>
                        <!-- .woocommerce -->
                    </div>
                </main>
                <!-- #main -->
            </div>
            <!-- .products-carousel -->
            </section>
            <!-- .section-products-carousel -->
        </div>
        <!-- .widget_techmarket_products_carousel_widget -->
    </div>
    <!-- #secondary -->
</div>
<!-- .row -->
