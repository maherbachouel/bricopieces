<div id="content" class="site-content ">
    <div class="col-full">
        <div class="row">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="home-v1-slider home-slider">
                        <div class="slider-1 slider-2" style="background-color:#f9f9f9;">
                            <img src="<?php img_file('slider/slider2.png') ?>" alt="Vente des pièces et accessoires d'électroménager, chauffage, climatisation, froid .">
                            <div class="caption">
                                <div class="title">Brico Pièces spécialisé dans la vente des pièces et accessoires d'électroménager, chauffage, climatisation, froid.</div>
								  <div class="sub-title">Rapide, fiable  service le jour même.<br>Disponible dans toutes l'île de Djerba.<br>Devis gratuits, instantané.</div>
								<a class="button" href="<?php echo base_url() . 'produits'; ?>">Obtenez maintenant   <i class="tm tm-long-arrow-right"></i></a>
                            </div>
                        </div>
                        <!-- .slider-1 -->
                        <div class="slider-1 slider-2" style="background-color:#f9f9f9;">
                            <img src="<?php img_file('slider/slider1.png') ?>" alt="Vente des différentes pièces de rechange">
                            <div class="caption">
                                <div class="title">Notre site Internet est reconnu dans la vente des différentes pièces de rechange à Djerba - Tunisie.
                                </div>
                                <div class="sub-title">Plus de 100 pièces pour réparer tous vos appareils.<br>Consultez nos catégories de pièces et découvrez notre large gamme de produits.</div>
                                <br>
                               <a class="button" href="<?php echo base_url() . 'produits'; ?>">Obtenez maintenant   <i class="tm tm-long-arrow-right"></i></a>
                            </div>
                        </div>
                        <!-- .slider-2 -->
                    </div>
                    <!-- .home-v1-slider -->
                    <div class="features-list">
                        <div class="features">
                            <!-- .feature -->
                            <div class="feature">
                                <div class="media">
                                    <i class="feature-icon d-flex mr-4 tm tm-feedback"></i>
                                    <div class="media-body feature-text">
                                        <h5 class="mt-0">99% client</h5>
                                        <span>Satisfait</span>
                                    </div>
                                </div>
                                <!-- .media -->
                            </div>
                            <!-- .feature -->
                            <div class="feature">
                                <div class="media">
                                    <i class="feature-icon d-flex mr-4 tm tm-free-return"></i>
                                    <div class="media-body feature-text">
                                        <h5 class="mt-0">365 jours</h5>
                                        <span>pour un retour gratuit</span>
                                    </div>
                                </div>
                                <!-- .media -->
                            </div>
                            <!-- .feature -->
                            <div class="feature">
                                <div class="media">
                                    <i class="feature-icon d-flex mr-4 tm tm-shopping-bag"></i>
                                    <div class="media-body feature-text">
                                        <h5 class="mt-0">6/7</h5>
                                        <span>Jours</span>
                                    </div>
                                </div>
                                <!-- .media -->
                            </div>
                            <!-- .feature -->
                            <div class="feature">
                                <div class="media">
                                    <i class="feature-icon d-flex mr-3 tm tm-best-brands"></i>
                                    <div class="media-body feature-text">
                                        <h5 class="mt-0">7:30h - 17:30h</h5>
                                        <span>/jours</span>
                                    </div>
                                </div>
                                <!-- .media -->
                            </div>
                            <!-- .feature -->
                        </div>
                        <!-- .features -->
                    </div>
                    <!-- /.features list -->
                </main>
				<section class="section-recent-posts-with-categories" style="padding-bottom:0em !important;">
					<div class="center-block " style="margin-bottom: 2em;margin-top: 2em;">
						<h2 class="section-title" style="font-size: 30px;">Brico Pièces, votre satisfaction est notre intérêt </h2>
						<div class="description">Chez <strong>Brico Pièces</strong>, nous vous donnerons l’attention et le service personnalisé que vous attendez de nous!</div>
						<div class="description">Nous sommes votre fournisseur de systèmes de chauffage, de ventilation et de climatisation. </div>
						<div class="description">Notre priorité est de satisfaire tous vos besoins en matière de réparation, d'installation et de maintenance de systèmes de <strong>chauffage et climatisation à Djerba.</strong></div>
						<div class="description"><strong>Brico Pièces </strong> est fière de fournir un service personnalisé et de haute qualité.</div>
						<div class="description">Nous sommes spécialisés dans la vente,  la réparation, le service et l’installation de tous les équipements électroménagers.</div>
						<div class="description">En ce qui concerne nos <a class="redirect" href="<?php echo base_url().'produits'?>">produits</a>, nous desservons la plupart des grandes marques.</div>
						<!--div class="description">
							<ul class="">
								<li class="nav-item">RAPIDE, FIABLE  Service le jour même.</li>
								<li class="nav-item">DISPONIBLE dans toutes l'île de Djerba</li>
								<li class="nav-item">CONSULTATION GRATUITE PAR TÉLÉPHONE ET DEVIS INSTANTANÉ sans obligation</li>
							</ul>
						</div-->
						<!-- .nav -->
					</div>
				</section>
                <div class="fullwidth-notice stretch-full-width">
                    <div class="col-full">
                        <p class="message">Climatisation - Électroménager - Chauffage - Froid</p>
                    </div>
                    <!-- .col-full -->
                </div>
                <!-- .fullwidth-notice -->
                <!-- .product-categories-carousel -->
                </section>
                <!-- /.section-landscape-products-carousel -->
                <section class="stretch-full-width section-products-carousel-with-vertical-tabs">
                    <header class="section-header">
                        <h2 class="section-title">
                            <strong>Produits </strong>les plus vus !</h2>
                    </header>
                    <!-- /.section-header -->
                    <div class="products-carousel-with-vertical-tabs row">
                        <ul role="tablist" class="nav display-non--tab">
                            <?php foreach ($recentsCategories as $cat) { ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:void(0)">
                                <span class="category-title">
                                    <span class="child-indicator"></span>
                                        <i class="fa fa-angle-right"></i> <?php echo $cat->name; ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                        <div style="background-color:#f9f9f9;; height: 552px;" class="tab-content">
                            <div class="tab-pane active" role="tabpanel">
                                <div class="products-carousel" data-ride="tm-slick-carousel" data-wrap=".products"
                                     data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:6,&quot;dots&quot;:true,&quot;arrows&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:2}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1600,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}}]}">
                                    <div class="container-fluid">
                                        <div class="woocommerce columns-5">
                                            <div class="products">
                                                <?php foreach ($produits as $product) { ?>
                                                    <div class="product">
                                                        <div class="yith-wcwl-add-to-wishlist">
                                                            <a href="javascript:void(0)" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                        </div>
                                                        <a href="<?php echo base_url() . 'produits/' . $product->category_permalink . '/' . $product->product_permalink; ?>" class="woocommerce-LoopProduct-link">
                                                            <img src="<?php echo base_url() . $product->product_banner ?>" width="224" height="197" class="wp-post-image" title="<?php echo $product->product_name; ?>" alt="<?php echo $product->product_name; ?>">
                                                            <span class="price">
                                                            <!--span class="amount">Prix: <?php echo $product->product_prix; ?></span-->
                                                            <ins>
                                                              <!--span class="amount"> TND</span-->
                                                            </ins>
                                                        </span>
                                                            <!-- /.price -->
                                                            <h2 class="woocommerce-loop-product__title"><?php echo $product->product_name; ?></h2>
                                                        </a>
                                                        <div class="hover-area">
                                                            <a class="button add_to_cart_button" href="<?php echo base_url() . 'produits/' . $product->category_permalink . '/' . $product->product_permalink; ?>" rel="nofollow">Voir Détails</a>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <!-- .woocommerce-->
                                    </div>
                                    <!-- .container-fluid -->
                                </div>
                                <!-- .products-carousel -->
                            </div>
                            <!-- .tab-pane -->
                            <!-- .tab-pane -->
                        </div>
                        <!-- .tab-content -->
                    </div>
                    <!-- /.products-carousel-with-vertical-tabs -->
                </section>
    