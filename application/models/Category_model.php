<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_Model extends MY_Model
{
    protected $table = 'category';

    public function AddCategory($data)
    {
        return $this->db->insert($this->table, $data);
    }
    public function EditCategory($data, $id)
    {
        return $this->db->where('id', $id)
            ->update($this->table, $data);
    }
    public function getCategoryById($id)
    {
        return $this->db->select('*')
            ->from($this->table)
            ->where('id', $id)
            ->get()
            ->result();
    }
    public function getCategoryByPermalink($permalink){
        return $this->db->select('*')
            ->from($this->table)
            ->where('category.permalink', $permalink)
            ->get()
            ->result();
    }
    public function getRecentCategory(){
        return $this->db->select('*')
            ->from($this->table)
            //->order_by('category.created_at', 'desc')
			->order_by('rand()')
            ->limit(4,0)
            ->get()
            ->result();
    }

}