<div class="page-title">
    <div class="title-env">
        <h1 class="title">Liste des emails </h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa-home"></i>Tableau de bord</a>
            </li>
            <li class="active ms-hover">
                <strong>Liste des emails</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="row">
        <div class="col-sm-12">
            <!--Basic Setup -->
            <div class="panel panel-default">
                <div class="panel-body">
                    <table id="admindatatable" class="dataTable table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($newsletter as $email) {
                            ?>
                            <tr>
                                <td><?php echo $email->id; ?></td>
                                <td>
                                    <?php echo $email->email; ?>
                                </td>
                                <td>
                                    <a href="#" class="confirm-modal" data-href="<?php echo base_url(); ?>admin/newsletter/delete"
                                       data-id="<?php echo $email->id; ?>"><i
                                            class="icon-admin fa-times-circle-o"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>