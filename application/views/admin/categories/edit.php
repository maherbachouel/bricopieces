<div class="page-title">
    <div class="title-env">
        <h1 class="title">Modifier une categorie</h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa-home"></i>Tableau de bord</a>
            </li>
            <li class="active ms-hover">
                <strong>Modifier une categorie</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <form role="form" enctype="multipart/form-data" method="post" class="form-horizontal" action="<?php echo base_url(); ?>admin/categories/edit/<?php echo $categorie->id; ?>">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nom</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control  keyup-meta" name="name" value="<?php echo $categorie->name; ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Meta Description</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="meta_description" value="<?php echo $categorie->meta_description; ?>" placeholder="Meta Description" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Permalink</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control permalink-set" value="<?php echo $categorie->permalink; ?>" name="permalink" placeholder="Permalink" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-grou text-right">
                <button class="btn btn-success btn-icon">
                    <i class="fa-check"></i>
                    <span>Enregistrer</span>
                </button>
            </div>
        </form>
    </div>
</div>