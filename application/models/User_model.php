<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends MY_Model
{
    protected $table = 'user';

    public function AuthenficationUser($email)
    {
        return $this->db->select('*')
            ->from($this->table)
            ->where('email', $email)
            ->get()
            ->result();
    }

    public function GetUserByIdKey($id,$api_key)
    {
        return $this->db->select('*')
            ->from($this->table)
            ->where($this->table . '.id', $id)
            ->where($this->table . '.api_key', $api_key)
            ->get()
            ->result();
    }
    public function EditProfile($data, $apiKey)
    {
        return $this->db->where('api_key', $apiKey)
            ->update($this->table, $data);
    }
    public function addUser($data)
    {
        return $this->db->insert($this->table, $data);
    }
}