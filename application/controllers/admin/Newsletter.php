<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_Controller.php');

class Newsletter extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('newsletter_model', 'newsletter');
    }


    public function index()
    {
        $data = array(
            "user" => $this->user,
            "activepage" => "newsletter",
            'newsletter' => $this->newsletter->read('*')
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/newsletter/liste', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }
    public function delete($id)
    {
        $this->newsletter->delete(array('id' => $id));
        redirect(base_url() . 'admin/newsletter');
    }
}
