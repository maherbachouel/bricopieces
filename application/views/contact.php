<div id="content" class="site-content">
    <div class="col-full">
        <div class="row">
            <nav class="woocommerce-breadcrumb">
                <a href="<?php echo base_url(); ?>">Accueil</a>
                <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                Contact
            </nav>
            <!-- .woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="type-page hentry">
                        <div class="entry-content">
                            <div class="stretch-full-width-map">
                                <iframe height="514" allowfullscreen="" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6630.79570196871!2d10.879855123779421!3d33.802044672698905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13aabb822f5e5e33%3A0x5265ffc63dc5cc51!2sEl+May!5e0!3m2!1sfr!2stn!4v1546094759239"></iframe>
                            </div>
                            <!-- .stretch-full-width-map -->
                            <div class="row contact-info">
                                <div class="col-md-9 left-col">
                                    <div class="text-block">
                                        <h2 class="contact-page-title">Laissez nous un message</h2>
                                    </div>
                                    <div class="contact-form">
                                        <div role="form" class="wpcf7" id="wpcf7-f425-o1" lang="en-US" dir="ltr">
                                            <div class="screen-reader-response"></div>
                                            <form class="wpcf7-form" method="post" action="<?php echo base_url() . 'contact' ?>">
                                                <div style="display: none;">
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-xs-12 col-md-6">
                                                        <label>Nom
                                                            <abbr title="required" class="required">*</abbr>
                                                        </label>
                                                        <br>
                                                        <span class="wpcf7-form-control-wrap first-name">
                                                                        <input type="text" required="" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text" size="40" value="" name="name">
                                                                    </span>
                                                    </div>
                                                    <!-- .col -->
                                                    <div class="col-xs-12 col-md-6">
                                                        <label>Email
                                                            <abbr title="required" class="required">*</abbr>
                                                        </label>
                                                        <br>
                                                        <span class="wpcf7-form-control-wrap last-name">
                                                                        <input type="email" required="" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text" size="40" value="" name="email">
                                                                    </span>
                                                    </div>
                                                    <!-- .col -->
                                                </div>
                                                <!-- .form-group -->
                                                <div class="form-group  row">
                                                    <div class="col-xs-12 col-md-6">
                                                        <label>Sujet<abbr title="required" class="required"> *</abbr></label>
                                                        <br>
                                                        <span class="wpcf7-form-control-wrap subject">
                                                                    <input type="text" required="" aria-invalid="false" class="wpcf7-form-control wpcf7-text input-text" size="40" name="subject">
                                                                </span>
                                                    </div>
                                                    <div class="col-xs-12 col-md-6">
                                                        <label>Téléphone<abbr title="required" class="required"> *</abbr></label>
                                                        <br>
                                                        <span class="wpcf7-form-control-wrap subject">
                                                                    <input type="number" required="" aria-invalid="false" class="wpcf7-form-control wpcf7-text input-text" size="40" name="phone">
                                                                </span>
                                                    </div>
                                                </div>
                                                <!-- .form-group -->
                                                <div class="form-group">
                                                    <label>VotreMessage<abbr title="required" class="required"> *</abbr></label>
                                                    <br>
                                                    <span class="wpcf7-form-control-wrap your-message">
                                                                    <textarea aria-invalid="false" required="" class="wpcf7-form-control wpcf7-textarea" rows="5" cols="40" name="message"></textarea>
                                                                </span>
                                                </div>
                                                <!--div class="g-recaptcha" style="margin-bottom: 20px" data-sitekey="6LfR4YUUAAAAAOMcU1aabjzNTajilTF40NLjkgBv"></div-->
                                                <!--div class="g-recaptcha" style="margin-bottom: 20px" data-sitekey="6Lfn34UUAAAAACygJb1GwlVPzuhWqohYwMOZlpNB"></div-->
                                                <!-- .form-group-->
                                                <div class="form-group clearfix">
                                                    <p>
                                                        <input type="submit" value="Envoyer le message" class="wpcf7-form-control wpcf7-submit"/>
                                                    </p>
                                                </div>
                                                <!-- .form-group-->
                                                <?php if ($notification === true) { ?>
                                                    <div class="wpcf7-response-output">
                                                        <p style="color:green;">Votre message a été envoyer avec succès.</p>
                                                    </div>
                                                <?php }
                                                if ($notification === false) { ?>
                                                    <div class="wpcf7-response-output">
                                                        <p style="color:red;">Votre message n'a pas été envoyé.</p>
                                                    </div>
                                                <?php } ?>
                                            </form>
                                            <!-- .wpcf7-form -->
                                        </div>
                                        <!-- .wpcf7 -->
                                    </div>
                                    <!-- .contact-form7 -->
                                </div>
                                <!-- .col -->
                                <div class="col-md-3 store-info">
                                    <div class="text-block">
                                        <h2 class="contact-page-title">Notre magasin</h2>
                                        <address>
                                            El May
                                            <br> Djerba - Medenin
                                        </address>
                                        <h3>Heures d'ouverture</h3>
                                        <ul class="list-unstyled operation-hours inner-right-md">
                                            <li class="clearfix">
                                                <span class="day">Lundi:</span>
                                                <span class="pull-right flip hours">7:30h - 17:30h</span>
                                            </li>
                                            <li class="clearfix">
                                                <span class="day">Mardi:</span>
                                                <span class="pull-right flip hours">7:30h - 17:30h</span>
                                            </li>
                                            <li class="clearfix">
                                                <span class="day">Mercredi:</span>
                                                <span class="pull-right flip hours">7:30h - 17:30h</span>
                                            </li>
                                            <li class="clearfix">
                                                <span class="day">Jeudi:</span>
                                                <span class="pull-right flip hours">7:30h - 17:30h</span>
                                            </li>
                                            <li class="clearfix">
                                                <span class="day">Vendredi:</span>
                                                <span class="pull-right flip hours">7:30h - 17:30h</span>
                                            </li>
                                            <li class="clearfix">
                                                <span class="day">Samedi:</span>
                                                <span class="pull-right flip hours">7:30h - 17:30h</span>
                                            </li>
                                            <li class="clearfix">
                                                <span class="day">Dimanche</span>
                                                <span class="pull-right flip hours">Fermé</span>
                                            </li>
                                        </ul>

                                        <h3>Carrières</h3>
                                        <p class="inner-right-md">Veuillez nous envoyer un email:<br>
                                            <a href="mailto:contact@brico-pieces.com">contact@brico-pieces.com</a> <br> <a href="mailto:bricopiecesjerba@gmail.com">bricopiecesjerba@gmail.com</a></p>
                                    </div>
                                    <!-- .text-block -->
                                </div>
                                <!-- .col -->
                            </div>
                            <!-- .contact-info -->
                        </div>
                        <!-- .entry-content -->
                    </div>
                    <!-- .hentry -->
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
        <!-- .row -->
    </div>
    <!-- .col-full -->
</div>