<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_Controller.php');

class Profile extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'usermodel');
    }


    public function index()
    {
        $data = array(
            "user" => $this->user,
            "activepage" => '',
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/profile/edit', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }

    public function edit($apiKey)
    {

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $user = array(
                'username' => $this->input->post('username', TRUE),
                'firstname' => $this->input->post('firstname', TRUE),
                'lastname' => $this->input->post('lastname', TRUE),
                'email' => $this->input->post('email', TRUE),
                'password' =>$this->encryption->encrypt($this->input->post('password', TRUE))
            );
            $this->usermodel->EditProfile($user,$apiKey);
            redirect(base_url() . 'admin/profile');
        }

    }
}
