<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pratique_Model extends MY_Model
{
    protected $table = 'practice';


    public function AddPratique($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function getPratiqueById($id)
    {
        return $this->db->select('*')
            ->from($this->table)
            ->where('id', $id)
            ->get()
            ->result();
    }

    public function EditPratique($data, $id)
    {
        return $this->db->where('id', $id)
            ->update($this->table, $data);
    }

    public function getAllPratique()
    {
        return $this->db->select('*')
            ->from($this->table)
            ->where(['status' => 1])
            ->order_by($this->table . '.created_at', 'desc')
            ->get()
            ->result();
    }

}