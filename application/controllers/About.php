<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('category_model', 'category');
    }
	public function index()
	{
		 $data = array(
            'activePage'=>'apropos',
             'filter'=>'',
             'categories' => $this->category->read('*'),
        );
        $view = $this->load->view('common/header', $data, TRUE);
        $view .= $this->load->view('apropos', $data, TRUE);
        $view .= $this->load->view('common/footer', $data, TRUE);
        echo $view;
	}
}
