<div class="page-title">
    <div class="title-env">
        <h1 class="title">Liste des produits</h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa-home"></i>Tableau de bord</a>
            </li>
            <li class="active ms-hover">
                <strong>Liste des produits</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="xe-widget xe-counter xe-counter-blue" data-count=".num" data-from="0" data-to="<?php echo count($produits); ?>" data-duration="2">
            <div class="xe-icon">
                <i class="linecons-truck"></i>
            </div>
            <div class="xe-label">
                <strong class="num"><?php echo count($produits); ?></strong>
                <span>Nombre des Produits</span>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="xe-widget xe-counter" data-count=".num" data-from="0" data-to="<?php echo $published; ?>" data-duration="2">
            <div class="xe-icon">
                <i class="linecons-truck"></i>
            </div>
            <div class="xe-label">
                <strong class="num"><?php echo $published; ?></strong>
                <span>Produits publier</span>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="xe-widget xe-counter xe-counter-red" data-count=".num" data-from="0" data-to="<?php echo $draft ; ?>" data-duration="2">
            <div class="xe-icon">
                <i class="linecons-truck"></i>
            </div>
            <div class="xe-label">
                <strong class="num"><?php echo $draft; ?></strong>
                <span>Produits non publier</span>
            </div>
        </div>
    </div>
</div>
<?php if (count($produits)) { ?>
    <div class="row">
        <div class="col-sm-12">
            <!--Basic Setup -->
            <div class="panel panel-default">
                <div class="panel-body">
                    <table id="admindatatable" class="dataTable table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Reference</th>
                            <th>Banner</th>
                            <th>Nom</th>
                            <th>Categorie</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($produits as $product) {
                            ?>
                            <tr>
                                <td>BP00<?php echo $product->product_id; ?></td>
                                <td>
                                    <img class="cover-admin" src="<?php echo base_url() . $product->product_banner; ?>">
                                </td>
                                <td><?php echo $product->product_name; ?></td>
                                <td><?php echo $product->category_name; ?></td>
                                <td><?php if ($product->product_status == 0) {
                                        echo "Draft";
                                    } else {
                                        echo "Published";
                                    } ?></td>
                                <td>
                                    <a href="<?php echo base_url(); ?>admin/produits/edit/<?php echo $product->product_id; ?>"
                                       data-id="<?php echo $product->product_id; ?>"><i
                                            class="icon-admin linecons-pencil"></i></a>
                                    <a href="javascript:void(0)" class="confirm-modal" data-href="<?php echo base_url(); ?>admin/produits/delete"
                                       data-id="<?php echo $product->product_id; ?>"><i
                                            class="icon-admin fa-times-circle-o"></i></a></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php } else {
    ?>
    <div class="no-data">
        <div class="no-data-icon">
            <i class="fa fa-exclamation"></i>
        </div>
        <p class="no-data-text">Aucun élément dans la liste</p>
        <a href="<?php echo base_url(); ?>admin/produits/add" class="no-data-link">Ajouter un produit</a>
    </div>
<?php } ?>