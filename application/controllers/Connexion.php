<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Connexion extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->library('encryption');
        $this->load->model('category_model', 'category');
    }

    public function index() {
		$notification = false;
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $email = strtolower($this->input->post('email', TRUE));
            $password = $this->input->post('password', TRUE);
            $user = $this->user->AuthenficationUser($email);
            if (count($user) == 1) {
                if ($this->encryption->decrypt($user[0]->password) == $password) {
                    $uid = $user[0]->id;
                    $api_key = $user[0]->api_key;
                    $role = $user[0]->role;
                    $this->session->set_userdata('loggedIn', $uid);
                    $this->session->set_userdata('uid', $uid);
                    $this->session->set_userdata('ukey', $api_key);
                    $this->session->set_userdata('role', $role);
                    $expire = time() + (86400 * 30);
                    setcookie('uid', $this->encryption->encrypt($uid . ' ' . $api_key), $expire, "/", $this->config->item('domain'), 0);
                    redirect(base_url().'admin/dashboard');
                }
            }
			$notification=true;
        }

        $loggedIn = (bool)$this->session->userdata('loggedIn');
        if ($loggedIn) {
            if ($this->session->userdata('role') === 'SUPER_ADMIN') {
                redirect(base_url().'admin/dashboard');
            }
            redirect(base_url());
        }
        $data = array(
            'activePage' => 'connexion',
            'filter' => '',
            'categories' => $this->category->read('*'),
            'notification' => $notification,
        );
        $view = $this->load->view('common/header', $data, TRUE);
        $view .= $this->load->view('connexion', $data, TRUE);
        $view .= $this->load->view('common/footer', $data, TRUE);
        echo $view;
    }

    public function login() {


        $data = array(
            'activePage' => 'connexion',
            'filter' => '',
            'categories' => $this->category->read('*'),
        );
        $view = $this->load->view('common/header', $data, TRUE);
        $view .= $this->load->view('connexion', $data, TRUE);
        $view .= $this->load->view('common/footer', $data, TRUE);
        echo $view;

    }

    public function logout() {
        $this->session->unset_userdata('loggedIn');
        $this->session->unset_userdata('uid');
        $this->session->unset_userdata('ukey');
        $this->session->unset_userdata('role');
        delete_cookie('uid', $this->config->item('domain'), "/");
        redirect(base_url());
    }

}
