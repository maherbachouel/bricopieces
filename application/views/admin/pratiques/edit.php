<div class="page-title">
    <div class="title-env">
        <h1 class="title">Modifier une pratique</h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa-home"></i>Tableau de bord</a>
            </li>
            <li class="active ms-hover">
                <strong>Modifier une pratique</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <form role="form" enctype="multipart/form-data" method="post" class="form-horizontal" action="<?php echo base_url(); ?>admin/pratiques/edit/<?php echo $pratique->id?>">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Titre</label>
                        <div class="col-sm-10">
                            <input type="text"  class="form-control" name="title" value="<?php echo $pratique->title;?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea class="form-control ckeditor" rows="10" name="description"><?php echo $pratique->description;?> </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10">
                            <select required class="form-control" name="status">
                                <option value="1" <?php if ($pratique->status==1){echo " selected"; }?>>Publier</option>
                                <option value="0" <?php if ($pratique->status==0){echo " selected"; }?>>Non Publier</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Vidéo</label>
                        <div class="col-sm-10">
                            <input  type="file" class="form-control" name="banner"
                                    accept="video/*"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-grou text-right">
                <button class="btn btn-success btn-icon">
                    <i class="fa-check"></i>
                    <span>Enregistrer</span>
                </button>
            </div>
        </form>
    </div>
</div>