</div>
</div>
</div>
</div>

<div id="left-social-link">
    <ul class="footer-social footer-social-2">
        <li>
            <a class="share" href="javascript:void(0)" target="_blank" title="Partager"><i class="fa fa-share-alt"></i></a>
        </li>
        <li>
            <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a>
        </li>
        <li>
            <a class="google-plus" href="https://plus.google.com/share?url=<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" target="_blank" title="Google Plus"><i class="fa fa-google-plus"></i></a>
        </li>
        <li>
            <a class="twitter" href="http://www.twitter.com/intent/tweet?text=<?php echo"https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a>
        </li>
        <li>
            <a class="pinterest" href="http://pinterest.com/pin/create/button/?url=<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" target="_blank" title="Pinterest"><i class="fa fa-pinterest"></i></a>
        </li>
    </ul>
</div>

<footer class="site-footer footer-v1">
<div class="menu-btn-anim"> <a class="btn btn-fill appeler-moi" title="Appelez-nous sur +21655646217" href="tel:+21655646217"><span class="fa fa-phone"></span></a></div>
    <div class="col-full">
        <div class="before-footer-wrap">
            <div class="col-full">
                <div class="footer-newsletter">
                    <div class="media">
                        <i class="footer-newsletter-icon tm tm-newsletter"></i>
                        <div class="media-body">
                            <div class="clearfix">
                                <div class="newsletter-header">
                                    <h5 class="newsletter-title">Inscrivez-vous à la newsletter</h5>
                                    <span class="newsletter-marketing-text">...pour
                                                    <strong>recevoir toutes les nouvelles</strong>
                                                </span>
                                </div>
                                <!-- .newsletter-header -->
                                <div class="newsletter-body">
                                    <form method="post" class="newsletter-form" id="subscribe-newsletter-form" action="<?php echo base_url() . 'accueil/subscribe' ?>">
                                        <input type="email" id="subscribeEmail" required placeholder="Entrez votre adresse email">
                                        <button class="button" type="submit" id="SubscribeNewsletter">S'inscrire</button>
                                    </form>
                                </div>
                                <!-- .newsletter body -->
                            </div>
                            <!-- .clearfix -->
                        </div>
                        <!-- .media-body -->
                    </div>
                    <!-- .media -->
                </div>
            </div>
            <!-- .col-full -->
        </div>
        <!-- .before-footer-wrap -->
        <div class="footer-widgets-block">
            <div class="row">
                <div class="footer-contact col-lg-4 col-md-4 col-sm-12">
                    <div class="footer-logo">
                        <a href="<?php echo base_url(); ?>" class="custom-logo-link" rel="home" title="Brico Pièces">
						<img class="logo-header" src="<?php echo base_url().'logo.png'; ?>"   alt="Brico Pièces" title="Brico Pièces" />
                        </a>
                    </div>
                    <!-- .footer-logo -->
                    <div class="contact-payment-wrap">
                        <div class="footer-contact-info">
                            <div class="media">
                                            <span class="media-left icon media-middle">
                                                <i class="tm tm-call-us-footer"></i>
                                            </span>
                                <div class="media-body">
                                    <span class="call-us-title">APPELEZ-NOUS !</span>
                                    <span class="call-us-text">(+216) 55 64 62 17</span>
                                    <address class="footer-contact-address">El May Djerba</address>
                                    <a href="<?php echo base_url() . 'contact'; ?>" class="footer-address-map-link">
                                        <i class="tm tm-map-marker"></i>Trouvez nous sur la carte</a>
                                </div>
                                <!-- .media-body -->
                            </div>
                            <!-- .media -->
                        </div>
                    </div>
                    <!-- .contact-payment-wrap -->
                </div>
                <div class="footer-widgets col-lg-4 col-md-4 col-sm-12">
                    <aside class="widget clearfix">
					
                        <div class="body">
                            <h4 class="widget-title sm-icon-label-link nav-link">Réseaux Sociaux</h4>
                            <div class="menu-footer-menu-1-container">
                                <ul id="menu-footer-menu-1" class="menu">
                                    <li class="menu-item">
                                        <a class="sm-icon-label-link nav-link" target="_blank" href="https://www.facebook.com/Bricopieces-Soci%C3%A9t%C3%A9-938765249480823/">
                                            <i class="fa fa-facebook"></i> Facebook</a>
                                    </li>
                                    <li class="menu-item">
                                        <a class="sm-icon-label-link nav-link" target="_blank" href="https://twitter.com/BricoPieces">
                                            <i class="fa fa-twitter"></i> Twitter</a>
                                    </li>
                                    <li class="menu-item">
                                        <a class="sm-icon-label-link nav-link" target="_blank" href="https://plus.google.com/106097678029802580182">
                                            <i class="fa fa-google-plus"></i> Google+</a>
                                    </li>
									<li class="menu-item">
                                        <a class="sm-icon-label-link nav-link" target="_blank" href="#">
                                            <i class="fa fa-instagram"></i> Instagram</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- .menu-footer-menu-1-container -->
                        </div>
                        <!-- .body -->
                    </aside>
                    <!-- .widget -->
                    <!-- .columns -->
                </div>
                <!-- .footer-contact -->
                <div class="footer-widgets col-lg-4 col-md-4 col-sm-12">
                    <aside class="widget clearfix">
					<div class="body">
                            <h4 class="widget-title sm-icon-label-link nav-link">Lien</h4>
                            <div class="menu-footer-menu-1-container">
                                <ul id="menu-footer-menu-1" class="menu">
                                    <li class="menu-item">
                                        <a class="sm-icon-label-link nav-link" href="<?php echo base_url() . 'services' ?>">
                                             Nos services</a>
                                    </li>
									<li class="menu-item">
                                <?php
                                $ci =& get_instance();
                                $loggedIn = (bool)$ci->session->userdata('loggedIn');
                                ?>
                                <?php if ($loggedIn) { ?>
                                    <a class="sm-icon-label-link nav-link"  title="Administrateur" href="<?php echo base_url() . 'connexion'; ?>">
                                         <span>Administrateur</span></a>
                                <?php } else { ?>
                                    <a class="sm-icon-label-link nav-link"  title="Connexion" href="<?php echo base_url() . 'connexion'; ?>">
                                        <span>Connexion</span></a>
                                <?php } ?>
                            </li>
                                </ul>
                            </div>
                            <!-- .menu-footer-menu-1-container -->
                        </div>
                        <div class="body">
                            <h4 class="widget-title sm-icon-label-link nav-link">Derniers articles</h4>
                            <div class="menu-footer-menu-1-container">
                                <ul id="menu-footer-menu-1" class="menu">
                                    <?php
                                    $ci =& get_instance();
                                    $ci->load->model('blog_model', 'blog');
                                    $articles = $ci->blog->getRecentArticle();
									if(count(articles)){
										foreach ($articles as $post) { ?>
                                        <li class="menu-item">
                                            <a class="sm-icon-label-link nav-link" href="<?php echo base_url() . 'blog/' . $post->permalink; ?>"><?php echo $post->title; ?></a>
                                        </li>
										<?php }}else{ ?>
										<li class="menu-item">
											<p>Aucun article trouvé</p>
										</li>
									<?php } ?>	
                                </ul>
								
                            </div>
                            <!-- .menu-footer-menu-1-container -->
                        </div>
                        <!-- .body -->
                    </aside>
                    <!-- .widget -->
                    <!-- .columns -->
                </div>
                <!-- .footer-widgets -->
            </div>
            <!-- .row -->
        </div>
        <!-- .footer-widgets-block -->
        <div class="site-info">
            <div class="col-full">
                <div class="copyright"> © <a href="<?php echo base_url(); ?>">Brico-pieces.com</a>
                    <script>document.write(new Date().getFullYear());</script>
                    . Tous droits réservés.
                </div>
            </div>
            <!-- .col-full -->
        </div>
        <!-- .site-info -->
    </div>
    <!-- .col-full -->
</footer>
<!-- .site-footer -->
<script type="text/javascript" src="<?php js_file('jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('tether.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('jquery-migrate.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('hidemaxlistitem.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('jquery-ui.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('hidemaxlistitem.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('jquery.easing.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('scrollup.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('jquery.waypoints.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('waypoints-sticky.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('pace.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('slick.min.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('scripts.js') ?>"></script>
<script type="text/javascript" src="<?php js_file('jquery.elevatezoom.js') ?>"></script>

<script>
    $('.zoom-img-produit').elevateZoom();
</script>
<!-- For demo purposes – can be removed on production : End -->
</body>

<!-- Mirrored from transvelo.github.io/techmarket-html/home-v1.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 27 Dec 2018 08:52:54 GMT -->
</html>