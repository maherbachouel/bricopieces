<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_Model extends MY_Model
{
    public function getResultSearch($keyword)
    {
        return array_merge($this->getSearchblog($keyword), $this->getSearchCategory($keyword), $this->getSearchProduct($keyword));

    }

    public function getSearchCategory($keyword)
    {
        $category = $this->db
            ->select('name ,permalink')
            ->from('category')
            ->like("category.permalink", $keyword)
            ->or_like("category.meta_description", $keyword)
            ->or_like("category.name", $keyword)
            ->get()
            ->result();

        $return = array();
        for ($i = 0; $i < count($category); $i++) {
            $category[$i]->permalink = base_url() .'produits/'. $category[$i]->permalink;
            $return = array_merge($return, array($category[$i]));
        }
        return $return;
    }

    public function getSearchblog($keyword)
    {
        $articles = $this->db
            ->select('description,title as name,permalink')
            ->from('blog')
            ->like("blog.description", $keyword)
            ->or_like("blog.meta_description", $keyword)
            ->or_like("blog.title", $keyword)
            ->get()
            ->result();

        $return = array();
        for ($i = 0; $i < count($articles); $i++) {
            $articles[$i]->permalink = base_url() . 'blog/' . $articles[$i]->permalink;
            $return = array_merge($return, array($articles[$i]));
        }
        return $return;
    }

    public function getSearchProduct($keyword)
    {
        $produits = $this->db
            ->select('product.description')
            ->select('product.name')
            ->select('product.permalink as product_permalink')
            ->select('category.permalink as category_permalink')
            ->from('product')
            ->join('category', 'product.category_id=category.id', 'left')
            ->where('product.status', 1)

            ->like("category.permalink", $keyword)
            ->or_like("category.name", $keyword)
            ->or_like("category.meta_description", $keyword)

            ->or_like("product.name", $keyword)
            ->or_like("product.permalink", $keyword)
            ->or_like("product.description", $keyword)
            ->or_like("product.meta_description", $keyword)
            ->get()
            ->result();

        $return = array();
        for ($i = 0; $i < count($produits); $i++) {
            $produits[$i]->permalink = base_url() . 'produits/' . $produits[$i]->category_permalink . '/' . $produits[$i]->product_permalink;
            $return = array_merge($return, array($produits[$i]));
        }
        return $return;
    }


}