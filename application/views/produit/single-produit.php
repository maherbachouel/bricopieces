<div id="content" class="site-content single-product full-width normal" tabindex="-1">
    <div class="col-full">
        <div class="row">
            <nav class="woocommerce-breadcrumb">
                <a href="<?php echo base_url(); ?>">Accueil</a>
                <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span><a href="<?php echo base_url() . 'produits/' . $product->category_permalink; ?>"><?php echo $product->category_name; ?></a>
                <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span><?php echo $product->product_name; ?>
            </nav>
            <!-- .woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="mains" class="site-main">
                    <div class="product product-type-simple">
                        <div class="single-product-wrapper">
                            <div class="product-images-wrapper thumb-count-4">
                                <!-- .onsale -->
                                <div id="techmarket-single-product-gallery" class="techmarket-single-product-gallery techmarket-single-product-gallery--with-images techmarket-single-product-gallery--columns-4 images" data-columns="4">
                                    <div class="techmarket-single-product-gallery-images">
                                        <div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4">
                                            <figure class="woocommerce-product-gallery__wrapper ">
                                                <div data-thumb="<?php echo base_url() . $product->banner; ?>" class="woocommerce-product-gallery__image">
                                                    <a href="javascript:void(0)" tabindex="0">
                                                        <img width="600" height="600" src="<?php echo base_url() . $product->banner; ?>" class="zoom-img-produit attachment-shop_single size-shop_single wp-post-image"title="<?php echo $product->product_name; ?>" alt="<?php echo $product->product_name; ?>">
                                                    </a>
                                                </div>

                                            </figure>
                                        </div>
                                        <!-- .woocommerce-product-gallery -->
                                    </div>
                                    <!-- .techmarket-single-product-gallery-images -->

                                    <!-- .techmarket-single-product-gallery-thumbnails -->
                                </div>
                                <!-- .techmarket-single-product-gallery -->
                            </div>
                            <!-- .product-images-wrapper -->
                            <div class="summary entry-summary">
                                <!-- .single-product-header -->
                                <div class="single-product-meta">
                                    <div class="cat-and-sku">
                                        <h4 class="posted_in categories">
                                            <a rel="tag" href="javascript:void(0)"><?php echo $product->product_name; ?></a>
                                        </h4>
                                        
                                    </div>
                                    
                                </div>
								  <div class="single-product-meta">
								  <span class="sku_wrapper">Référence : 
                                                        <span class="sku">BP00<?php echo $product->product_id; ?></span>
                                                    </span> 
													
													<div class="product-label">
                                        <div class="ribbon label green-label">
                                            <span>Date de création : <?php echo date("d/M", strtotime($product->product_created_at)); ?></span>
                                        </div>
										<div class="ribbon label green-label">
                                            <span>Disponibilité : en stock </span>
                                        </div>
                                    </div>
								  </div>
                                <!-- .rating-and-sharing-wrapper -->
                                <div class="woocommerce-product-details__short-description">
                                    <?php echo $product->product_description; ?>
                                </div>
                                <!-- .woocommerce-product-details__short-description -->
                                <!--div class="product-actions-wrapper">
                                    <div class="product-actions">
                                        <p class="price">
                                            <ins>
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">Prix: <?php echo $product->product_prix; ?></span> TND</span>
                                            </ins>
                                        </p>

                                    </div>
                                </div-->
                                <!-- .product-actions-wrapper -->
                            </div>
                            <!-- .entry-summary -->
                        </div>
                        <!-- .single-product-wrapper -->
                        <!-- .single-product-wrapper -->
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>