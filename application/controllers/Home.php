<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('category_model', 'category');
        $this->load->model('product_model', 'product');
    }

	public function index()
	{
		

		 $data = array(
            'activePage'=>'home',
             'filter'=>'',
             'categories' => $this->category->read('*'),
             'recentsCategories' => $this->category->getRecentCategory('*'),
             'produits' => $this->product->getRecentProduct()
        );
		
        $view = $this->load->view('common/header', $data, TRUE);
        $view .= $this->load->view('home', $data, TRUE);
        $view .= $this->load->view('common/footer', $data, TRUE);
        echo $view;
	}

    public function subscribe()
    {
        $this->load->model('newsletter_model', 'newsletter');
        $result = ['success' => false];
        $email = $this->input->get('email', TRUE);
        if ($email) {
            if (count($this->newsletter->readNewsletter('id', ['email' => $email])) == 0) {
                $this->newsletter->createNewsletter(['email' => $email]);
                $result['success'] = true;
            }
        }
        echo json_encode($result);
        exit();

    }
}
