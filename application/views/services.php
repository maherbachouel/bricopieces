<div id="content" class="site-content">
    <div class="col-full">
        <div class="row">
            <nav class="woocommerce-breadcrumb">
                <a href="<?php echo base_url(); ?>">Accueil</a>
                <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                Services
            </nav>
            <!-- .woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="type-page hentry">
                        <header class="entry-header">
                            <div class="page-header-caption">
                                <h1 class="entry-title">Tous nos services</h1>
                            </div>
                        </header>
                        <div class="about-accordion col-md-12 col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                   <section class="section-recent-posts-with-categories">
                                    <div class="center-block">
                                        <div class="description"><strong>Brico Pièces</strong> fournit des services de réparation et d’entretien rapides, fiables et le jour même aux particuliers et aux entreprises. Depuis sa création, nos experts en chaud, ventilation et climatisation, formés et compétents, dotés d'une expérience professionnelle, fournissent des services dédiés et haut de gamme à la communauté.</div>
                                        <div class="description">Notre service va au-delà de la réparation et de l'entretien de la climatisation, mais nous pouvons vous aider pour tout problème que vous pourriez avoir et pour établir une relation professionnelle durable.</div>
                                        <ul class="nav nav-inline categories">
                                            <li class="nav-item"><a href="<?php echo base_url() . 'services/climatisation'; ?>" class="nav-link">Climatisation </a></li>
                                            <li class="nav-item"><a href="<?php echo base_url() . 'services/chauffage'; ?>" class="nav-link">Chauffage </a></li>
                                            <li class="nav-item"><a href="<?php echo base_url() . 'services/electro-menager'; ?>" class="nav-link">Électroménager </a></li>
                                            <li class="nav-item"><a href="<?php echo base_url() . 'services/froid'; ?>" class="nav-link">Froid</a></li>
                                        </ul>
                                        <!-- .nav -->
                                    </div>
									</section>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>