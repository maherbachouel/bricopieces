<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_Controller.php');

class Pratiques extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('pratique_model', 'pratique');
    }


    public function index()
    {
        $data = array(
            "user" => $this->user,
            "activepage" => "pratiques",
            'pratiques' => $this->pratique->read('*'),
            'published' =>  $this->pratique->count(array("status" => 1)),
            'draft' =>  $this->pratique->count(array("status" => 0))
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/pratiques/liste', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            $data = array(
                "title" => $this->input->post('title'),
                "description" => $this->input->post('description'),
                "status" => $this->input->post('status'),
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );
            if (isset($_FILES['banner']['name']) && $_FILES['banner']['name']) {
                $upload_dir = FCPATH . 'uploads/';
                $formations_dir = $upload_dir . 'practice/';
                $image_dir = 'uploads/practice/';
                if (!is_dir($upload_dir)) {
                    mkdir($upload_dir);
                }
                if (!is_dir($formations_dir)) {
                    mkdir($formations_dir);
                }
                $filename = time();
                $extension = strtolower(pathinfo($_FILES['banner']['name'], PATHINFO_EXTENSION));
                $image = $filename . '.' . $extension;

                if (!move_uploaded_file($_FILES['banner']['tmp_name'], $formations_dir . $image)) {
                    $errors[] = 'ATTACHMENT_FILE_NOT_VALID';
                    $image = null;
                } else {
                    $data['banner'] = $image_dir.$image;
                }
            }

            $this->pratique->AddPratique($data);
            redirect(base_url() . 'admin/pratiques');
        }
        $data = array(
            "user" => $this->user,
            "activepage" => "pratiques",
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/pratiques/add', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }
    public function edit($id){

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $data = array(
                "title" => $this->input->post('title'),
                "description" => $this->input->post('description'),
                "status" => $this->input->post('status'),
                "updated_at" => date('Y-m-d H:i:s')
            );
            if (isset($_FILES['banner']['name']) && $_FILES['banner']['name']) {
                $upload_dir = FCPATH . 'uploads/';
                $formations_dir = $upload_dir . 'practice/';
                $image_dir = 'uploads/practice/';
                if (!is_dir($upload_dir)) {
                    mkdir($upload_dir);
                }
                if (!is_dir($formations_dir)) {
                    mkdir($formations_dir);
                }
                $filename = time();
                $extension = strtolower(pathinfo($_FILES['banner']['name'], PATHINFO_EXTENSION));
                $image = $filename . '.' . $extension;

                if (!move_uploaded_file($_FILES['banner']['tmp_name'], $formations_dir . $image)) {
                    $errors[] = 'ATTACHMENT_FILE_NOT_VALID';
                    $image = null;
                } else {
                    $data['banner'] = $image_dir.$image;
                }
            }
            $this->pratique->EditPratique($data, $id);
            redirect(base_url() . 'admin/pratiques');
        }

        if (!$this->pratique->getPratiqueById($id)) {
            redirect(base_url() . 'admin/pratiques');
        }
        $data = array(
            "user" => $this->user,
            "pratique" => $this->pratique->getPratiqueById($id)[0],
            "activepage" => 'pratiques'
        );
        $view = $this->load->view('admin/common/header', $data, TRUE);
        $view .= $this->load->view('admin/pratiques/edit', $data, TRUE);
        $view .= $this->load->view('admin/common/footer', $data, TRUE);
        echo $view;
    }
    public function delete($id)
    {
        $this->pratique->delete(array('id' => $id));
        redirect(base_url() . 'admin/pratiques');
    }
}
