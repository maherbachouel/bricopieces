<div id="content" class="site-content blog-list ">
    <div class="col-full">
        <div class="row">
            <nav class="woocommerce-breadcrumb">
                <a href="<?php echo base_url(); ?>">Accueil</a>
                <span class="delimiter">
                                <i class="fa fa-angle-right"></i>
                            </span>
                Blog
            </nav>
            <!-- .woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <?php
                    if(count($articles)){
                    foreach ($articles as $post) { ?>
                        <article class="post format-image hentry">
                            <div class="media-attachment">
                                <div class="post-thumbnail">
                                    <a href="<?php echo base_url() . 'blog/' . $post->permalink; ?>">
                                        <img width="460" height="244" title="<?php echo $post->title; ?>" alt="<?php echo $post->title; ?>" class="wp-post-image" src="<?php echo base_url() . $post->banner ?>">
                                    </a>
                                </div>
                            </div>
                            <!-- .media-attachment -->
                            <div class="content-body">
                                <header class="entry-header">
                                    <h1 class="entry-title">
                                        <a rel="bookmark" href="<?php echo base_url() . 'blog/' . $post->permalink; ?>"><?php echo $post->title; ?></a>
                                    </h1>
                                    <div class="entry-meta">
                                        <!-- .cat-links -->
                                        <span class="posted-on">
                                                    <a rel="bookmark" href="javascript:void(0)">
                                                        <time class="entry-date published"><?php echo date("d/M/Y", strtotime($post->updated_at)); ?></time>
                                                    </a>
                                                </span>
                                        <!-- .posted-on -->
                                        <span class="author">
                                                    <a rel="author" title="Admin" href="javascript:void(0)">Admin</a>
                                                </span>
                                        <!-- .author -->
                                    </div>
                                    <!-- .entry-meta -->
                                </header>
                                <!-- .entry-header -->
                                <div class="entry-content">
                                    <p><?php echo strlen($post->description) > 250 ? substr($post->description, 0, 250) . '...' : $post->description; ?></p>
                                </div>
                                <!-- .entry-content -->
                                <div class="post-readmore">
                                    <a class="btn btn-primary" href="<?php echo base_url() . 'blog/' . $post->permalink; ?>">Lire la suite</a>
                                </div>

                            </div>
                            <!-- .content-body -->
                        </article>
                    <?php }}else{ ?>
                        <div class="liste-not-found">
                            <p>Aucune article dans la liste</p>
                        </div>
                    <?php } ?>
                    <!-- .navigation -->
                </main>
                <!-- #main -->
            </div>
        </div>
        <!-- .sidebar-blog -->
    </div>
    <!-- .row -->
</div>